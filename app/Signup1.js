
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  AsyncStorage,
  ActivityIndicator,
  Alert
} from 'react-native';
import { Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Indicator from './components/Steps';
import Header from './components/Header1';
import Api from './controller/api';
import Geocoder from 'react-native-geocoding';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

var lat
var long
var gpsclick=false
export default class Signup1 extends Component {
  constructor(props){
    super(props);
    this.state={
      email:'',
      emailError:'',
      username:'',
      password:'',
      phoneNumber: '',
      address:'',
      currentPage:1,
      emailnull:false,
      usernamenull:false,
      passwordnull:false,
      phoneNumbernull:false,
      addressnull:false,
      indicator: false,
    

    }
  }
    componentDidMount(){
      Geocoder.init('AIzaSyCAOFImrlK9C7-wmmqxz44KTs8ebW8BCHg');
      if(Platform.OS === 'android'){
        LocationServicesDialogBox.checkLocationServicesIsEnabled({ 
          message: "<h2>Use Location?</h2> \
                      This app wants to change your device settings:<br/><br/>\
                      Use GPS for location<br/><br/>", 
          ok: "YES", 
          cancel: "NO" 
      }).then(() => { 
        navigator.geolocation.getCurrentPosition((position) => {
          lat = parseFloat(position.coords.latitude)
          long = parseFloat(position.coords.longitude)
         console.log('lat',lat)
         console.log('long',long)
       
            }, (error) => console.log(JSON.stringify(error)),
         { enableHighAccuracy:true, timeout: 20000 })
      })
      }
      else{
          navigator.geolocation.getCurrentPosition((position) => {
            lat = parseFloat(position.coords.latitude)
            long = parseFloat(position.coords.longitude)
          console.log('lat',lat)
          console.log('long',long)
        
              }, (error) => console.log(JSON.stringify(error)),
          { enableHighAccuracy:true, timeout: 20000 })
        
      }
     
      

    }

    _getAddress(){
           gpsclick=true
  //       fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+lat + ',' + long + 'key=AIzaSyAXfqKtg1eihJdmbN-IugX2r3xtILh5b5Y')
  //        .then((response) =>  response.json())
  //        .then((result) => {
            
  //            console.log(result)
  //            this.setState({address:result.results[0].formatted_address})
  //            console.log(this.state.address)
             
  // })
        Geocoder.init('AIzaSyCAOFImrlK9C7-wmmqxz44KTs8ebW8BCHg');
        if(Platform.OS === 'android'){
          LocationServicesDialogBox.checkLocationServicesIsEnabled({ 
            message: "<h2>Use Location?</h2> \
                        This app wants to change your device settings:<br/><br/>\
                        Use GPS for location<br/><br/>", 
            ok: "YES", 
            cancel: "NO" 
        }).then(() => { 
        Geocoder.from(lat,long)
        .then(json => { console.log(json)
                var addressComponent = json.results[0].address_components[0];
                this.setState({address:json.results[0].formatted_address})
          console.log(addressComponent);
        })
        .catch(error => console.warn(error));
      })
      }
      else{
        Geocoder.from(lat,long)
        .then(json => { console.log(json)
                var addressComponent = json.results[0].address_components[0];
                this.setState({address:json.results[0].formatted_address})
          console.log(addressComponent);
        })
        .catch(error =>  {Alert.alert("Use Location?","Please Enable Your Location..!"),gpsclick=false});
      }
    }

  signUp(){
  
   
    if(this.state.email == '' )
    {
      this.setState({emailnull:true})
    }
    else if(this.state.username == '')
    {
      this.setState({emailnull:false,usernamenull:true})
    }
    else if(this.state.password == '')
    {
      this.setState({passwordnull:true,usernamenull:false})
    }
    else if(this.state.phoneNumber == '')
    {
      this.setState({phoneNumbernull:true,passwordnull:false})
    }
    else if(this.state.address == '')
    {
   
      this.setState({addressnull:true,phoneNumbernull:false})
    }
    else if(gpsclick!=true){
      Alert.alert('Use Location','Please enable your location service and use location icon in order to input your correct location.')
    }
    else
    {
     this.setState({emailnull:false,passwordnull:false,usernamenull:false,phoneNumbernull:false,addressnull:false})
     this.setState({indicator:true})
     var formData = new FormData()
     formData.append('user_name',this.state.username);
     formData.append('email',this.state.email);
     formData.append('phone_number',this.state.phoneNumber);
     formData.append('password',this.state.password);
     formData.append('address',this.state.address);

     Api.post('app/api/signup',formData)
     .then(result => {console.log('result',result)
          this.setState({userID:result.user_id})
 
         if(result.message == 'Success.')
         {
              AsyncStorage.multiRemove(['user_id','email','user_name','password','address','phone_number'])
              AsyncStorage.setItem('email',this.state.email)
              AsyncStorage.setItem('user_name',this.state.username)
              AsyncStorage.setItem('password',this.state.password)
              AsyncStorage.setItem('address',this.state.address)
              AsyncStorage.setItem('phone_number',this.state.phoneNumber.toString())
             this.setState({indicator:false})
             Actions.confirmNumber({id:result.user_id})
         }
         else
         {
             this.setState({indicator:false})
             this.setState({emailError: result.message})
         }
 
     })
     .catch(error => {console.error(error)});
  }
 }

  render() {
    return (
      <View style={styles.container}>
        <Header/>
          {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
          <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
          {this.state.indicator == true ? <View style={{ flex: 1,justifyContent: 'center'}}><ActivityIndicator size="large" color="#ea0017" /></View> :

        <ScrollView>
          
        <View style={styles.indicator}>
          {/* <Indicator var={this.state.currentPage}/> */}
        </View>
        <View style={{padding:10}}>
          <View style={{justifyContent:'center',alignItems:'center'}}>
            <Image source={require('./images/products/registration.png')}  style={{height:22.5,width:121}} />
            <Text style={{marginLeft:5,width:'80%',textAlign:'center',marginTop:20,marginBottom:20}}>Welcome to Valkyrie application. You need to register to start using the appliaction</Text>
            <View style={this.state.emailnull != true ? styles.inputView : styles.errorView}>
              <Icon name="ios-mail-outline" style={{padding:8,color:'#ea0017',fontSize:20,alignSelf:'flex-start'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="Email" 
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({email:text})} />
            </View>
            {this.state.emailError != '' ? <Text style={{color:'red',marginTop:this.state.emailError != null ? 3 : 0}}>{this.state.emailError}</Text>: null}
            <View style={this.state.usernamenull != true ? styles.inputView : styles.errorView}>
            <Icon name="ios-person-outline" style={{padding:8,fontSize:25, color:'#ea0017'}}/>
              <TextInput  
                style={{width:200,}}
                placeholder="User Name"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({username:text})} />
            </View>
            <View style={this.state.passwordnull != true ? styles.inputView : styles.errorView}>
            <Icon name="ios-key-outline" style={{padding:8,fontSize:20, color:'#ea0017'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="********"
                underlineColorAndroid="transparent"
                secureTextEntry={true}
                onChangeText={(text) => this.setState({password:text})} />
            </View>
            <View style={this.state.phoneNumbernull != true ? styles.inputView : styles.errorView}>
            <Icon name="ios-call-outline" style={{padding:8,fontSize:25, color:'#ea0017'}}/>
              <Text style={{paddingTop:Platform.OS == 'android' ? 8 : 10}} >+962 | </Text>
              <TextInput  
                keyboardType="numeric"
                style={{width:200}}
                placeholder="Phone Number"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({phoneNumber:'+962'+text})} />
            </View>
            <View style={this.state.addressnull != true ? styles.inputView : styles.errorView}>
            <Icon type="EvilIcons" name="location" style={{padding:6,fontSize:22, color:'#ea0017'}}/>
              <TextInput  
                style={{width:'70%'}}
                placeholder="Address"
                value = {this.state.address}
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({address:text})} />
              <TouchableOpacity onPress={()=> this._getAddress()} ><Icon type="MaterialIcons" name="my-location" style={{padding:6,fontSize:22, color:'#ea0017'}} /></TouchableOpacity>
            </View>
          </View>
            <View style={styles.btnView}>
                <TouchableOpacity style={styles.btn} onPress={() => this.signUp()} >
                    <Text style={{color:'white',fontSize:16,fontWeight:'500',textAlign:'center'}}>Registration</Text>
                </TouchableOpacity>
                <View style={{margin:20}}>
                    <Text>Already have an account ? <Text style={{color:'#ea0017'}} onPress={()=>Actions.login()} >Sign in</Text></Text>
                </View>
                <View>
                    {/* <View style={{flexDirection:'row',margin:5}}>
                        <Icon type="FontAwesome" name="facebook-square" style={{color:'#ea0017',fontSize:20}}/>
                        <Text style={{marginLeft:5}}>www.facebook.com/valkyrie/Laundry</Text>
                    </View>    
                    <View style={{flexDirection:'row',margin:5}}>
                        <Icon type="FontAwesome" name="instagram" style={{color:'#ea0017',fontSize:20}}/>
                        <Text style={{marginLeft:5}}>www.instagram.com/valkyrie/Laundry</Text>
                    </View>  */}
                </View>
            </View>
        </View>
        </ScrollView>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor:'#ea0017'
  },
  indicator:{
    padding:20,

  },
  errorView:{
    flexDirection:'row',
    padding:5,
    borderColor:'#ea0017',
    borderWidth:1,
    width:'70%',
    height:50,
    marginTop:10,
    borderRadius:5
  },
  inputView:{
    flexDirection:'row',
    padding:5,
    borderWidth:1,
    width:'70%',
    height:50,
    marginTop:10,
    borderRadius:5
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  btn:{
    backgroundColor:'#282423',
    height:50,
    width:'70%',
    justifyContent:'center',
    borderRadius:5
  }
});
