
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Header from './components/Header1';
import Api from './controller/api';

export default class ForgetPassword1 extends Component {
  constructor(props){
    super(props);
    this.state={
      email:this.props.email,
      verificationCode:'',
      newPassword: '',
      error:''
    }
  }

    _forgetPassword(){

      var formData = new FormData()
      formData.append('email',this.state.email);
      formData.append('password',this.state.newPassword);
      formData.append('verification_code',this.state.verificationCode)

    Api.post('app/api/forgotPassword',formData)
    .then(result => {console.log('result',result)

        if(result.message == 'Success.')
        {       
            this.setState({error:null})
            Actions.login()
           
        }
        else
        {
            console.log(result.message)
            this.setState({error: result.message})
        }

      
    })
    .catch(error => {console.error(error)});
    }

  render() {
    return (
      <View style={styles.container}>
       <Header/>
        {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
        <View style={{marginTop:'20%'}}>
          <View style={{justifyContent:'center',alignItems:'center'}}>
           <Text style={{fontWeight:'400',fontSize:18,width:'80%'}}>Please input varification code and new password</Text>
           <TextInput style={{width:'65%',height:40,marginTop:30}} onChangeText={(text)=>this.setState({verificationCode:text})} placeholder="Verification Code" />
           <TextInput style={{width:'65%',height:40,marginTop:10}} placeholder="New Password" onChangeText={(text) => this.setState({newPassword:text})} />
      
           {this.state.error != '' ? <Text style={{color:'red',marginTop:3}}>{this.state.error}</Text> : null }
          </View>
          <View style={styles.btnView}>
            <TouchableOpacity style={styles.btn} onPress={() => this._forgetPassword()} >
              <Text style={{color:'white',fontSize:20,fontWeight:Platform.OS == 'android'  ? '500' : '800',textAlign:'center'}}>Reset</Text>
            </TouchableOpacity>
          </View> 
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor:'#ea0017'
  },
  image:{
    alignItems:'center',
    margin:20
  },
  inputView:{
    flexDirection:'row',
    padding:10,
    borderWidth:0.5,
    width:'70%',
    marginTop:10,
    borderRadius:10
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  btn:{
    backgroundColor:'#ea0017',
    height:40,
    width:'60%',
    justifyContent:'center',
    borderRadius:5,
    marginTop:10
  }
});
