class Api {
  static headers() {
    return {
      'Accept': 'application/json',
    }
  }

  static get(route) {
    return this.Valkyrie(route, null, 'GET');
  }

  static put(route, params) {
    return this.Valkyrie(route, params, 'PUT')
  }

  static post(route, params) {
    //console.log('inside post',this.Valkyrie(route, params, 'POST'))
    return this.Valkyrie(route, params, 'POST')
  }
  static postWithToken(route, params) {
    return this.Valkyrie(route, params, 'POST',)
  }

  static delete(route, params) {
    return this.Valkyrie(route, params, 'DELETE')
  }

  static Valkyrie(route, params,verb) {
    const host = 'http://52.66.111.241/'
  
    const url = `${host}${route}`
    console.log(url)
    
    //let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
    console.log(params)
    let options = Object.assign({ method:verb }, params ? {body:  params } : null);
    options.headers = Api.headers()
    //console.log('---------Data inside api-----',params)
    //console.log('-------header called-------',Api.headers(token))
   
    return fetch(url, options).then( resp => {
     // console.log('-----my Response-----',resp)
      let json = resp.json();
      
   // console.log('-----got the resp onject-----',resp)
      if (resp.ok) {
        //console.log('-----inside api-----',resp)
        return json;
          //console.log('-----inside api-----',resp)
      }
      else{
        return json;
      }
      return json.then(err => {throw err});

    }).then( json => json);
  }
}
export default Api
