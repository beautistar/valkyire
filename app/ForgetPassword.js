
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Header from './components/Header1';
import Api from './controller/api';

export default class ForgetPassword extends Component {
  constructor(props){
    super(props);
    this.state={
      email:'',
      phoneNumber: '',
      error:''
    }
  }

    _forgetPassword(){

      var formData = new FormData()
      formData.append('phone_number',this.state.phoneNumber);
      formData.append('email',this.state.email);

    Api.post('app/api/validCustomer',formData)
    .then(result => {console.log('result',result)

        if(result.message == 'Success.')
        {       
            this.setState({error:null})
            Actions.forgetPassword1({email:this.state.email})
           
        }
        else
        {
            console.log(result.message)
            this.setState({error: result.message})
        }

      
    })
    .catch(error => {console.error(error)});
    }

  render() {
    return (
      <View style={styles.container}>
       <Header/>
        {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
        <View style={{marginTop:'20%'}}>
          <View style={{justifyContent:'center',alignItems:'center'}}>
           <Text style={{fontWeight:'400',fontSize:18,width:'80%'}}>Please input your email and phone number which is used in this app</Text>
           <TextInput style={{width:'62%',height:40,marginTop:30,marginLeft:10}} onChangeText={(text)=>this.setState({email:text})} placeholder="abc@example.com" />
           <View style={{flexDirection:'row',marginLeft:Platform.OS == 'android' ? -35 : -35}}>
           <Text style={{marginTop:Platform.OS == 'android' ? 10 : 0,paddingTop:5 ,textAlign:'center'}} >+962 | </Text>
              <TextInput  
                keyboardType="numeric"
                style={{width:'56%'}}
                placeholder="Phone Number"
                onChangeText={(text) => this.setState({phoneNumber:'+962'+text})} />
           </View>
           {this.state.error != '' ? <Text style={{color:'red',marginTop:3}}>{this.state.error}</Text> : null }
          </View>
          <View style={styles.btnView}>
            <TouchableOpacity style={styles.btn} onPress={() => this._forgetPassword()} >
              <Text style={{color:'white',fontSize:20,fontWeight:Platform.OS == 'android' ? '500' : '800',textAlign:'center'}}>Next</Text>
            </TouchableOpacity>
          </View> 
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor:'#ea0017'
  },
  image:{
    alignItems:'center',
    margin:20
  },
  inputView:{
    flexDirection:'row',
    padding:10,
    borderWidth:0.5,
    width:'70%',
    marginTop:10,
    borderRadius:10
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  btn:{
    backgroundColor:'#ea0017',
    height:40,
    width:'60%',
    justifyContent:'center',
    borderRadius:5,
    marginTop:10
  }
});
