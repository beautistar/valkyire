import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
  StatusBar,
  ActivityIndicator
} from 'react-native';
import { Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Header from './components/Header1';
import Splashscreen from './components/Splashscreen' ;
import Api from './controller/api';
import  FBSDK,{LoginManager,AccessToken} from 'react-native-fbsdk';


export default class App extends Component {
  constructor(props){
    super(props);
    this.state={
      // email:'test5251@user.com',
      // password:'123456',
      email:'',
      password:'',
      emailnull:false,
      passwordnull:false,
      error:'',
      timePassed: false,
      indicator: false
    }
  }
  componentDidMount() {
    setTimeout( () => {
      this.setTimePassed();
   },2000);
   AsyncStorage.getItem('user_id')
   .then((id) => { console.log(id), id != null ? Actions.drawer() : null})
  }
  setTimePassed() {
    this.setState({timePassed: true});

 }

  _fblogin(){

    LoginManager.logInWithReadPermissions(['public_profile','email']).then(
      function(result) {
        if (result.isCancelled) {
          alert('Login cancelled');
        } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              console.log(data)
              let token = data.accessToken;
              //console.log(token)

              fetch('https://graph.facebook.com/v3.0/me?fields=email,name&access_token=' + token)
              .then((resp) => resp.json())
              .then((result) => {
                console.log('facebook',result)

                var formdata = new FormData
                formdata.append('facebook_id',result.id)

                Api.post('app/api/checkActive',formdata)
                .then(res => { console.log(res)

                  if(res.user_model.address && res.user_model.phone_number && res.user_model.is_active == "Yes") 
                  {
                    AsyncStorage.multiRemove(['user_id','email','user_name','password','address','phone_number'])
                    AsyncStorage.setItem('user_id',res.user_model.user_id)
                    AsyncStorage.setItem('email',res.user_model.email)
                    AsyncStorage.setItem('user_name',res.user_model.user_name)
                    AsyncStorage.setItem('address',res.user_model.address)
                    AsyncStorage.setItem('phone_number',res.user_model.phone_number)
                    Actions.drawer()
                  }
                  else
                  {
                    Actions.add({user_name:result.name,id:result.id,email:result.email});
                  }
                })
              })
         
              })
        }
      },
      function(error) {
        alert('Login fail with error: ' + error);
      }
    );
  }

  login=()=>{
    this.setState({indicator:true})
    
    var formData = new FormData()
    formData.append('email',this.state.email);
    formData.append('password',this.state.password);

    Api.post('app/api/signin',formData)
    .then(result => {console.log('result',result)
        if(result.message == 'Success.')
        {
            AsyncStorage.multiRemove(['user_id','email','user_name','password','address','phone_number'])
            AsyncStorage.setItem('user_id',result.user_model.user_id)
            AsyncStorage.setItem('email',result.user_model.email)
            AsyncStorage.setItem('user_name',result.user_model.user_name)
            AsyncStorage.setItem('password',this.state.password)
            AsyncStorage.setItem('address',result.user_model.address)
            AsyncStorage.setItem('phone_number',result.user_model.phone_number)
            console.log(result.user_model.user_id)
            this.setState({indicator:false})
            Actions.drawer()
        }
        else
        {
            this.setState({indicator:false})
            this.setState({error: result.message})
        }

    })
    .catch(error => {console.error(error)});


  }
  render() {
 
    if(!this.state.timePassed){
     
      return (
        
      <Splashscreen />
    )
    }
    else{
     
     return (
      
      <View style={styles.container}>
        <Header/>
        <StatusBar backgroundColor='#ea0017'  barStyle={Platform.OS=='android'?"light-content":null}/> 
        {this.state.indicator == true ? <View style={{ flex: 1,justifyContent: 'center'}}><ActivityIndicator size="large" color="#ea0017" /></View> :
        <ScrollView>
        <View style={styles.image}>
            <Image source={require('./images/logo.png')} style={{height:130,width:160}}/>
        </View>
        <View>
          <View style={{justifyContent:'center',alignItems:'center'}}>
            <View style={styles.inputView}>
            <Icon name="ios-person-outline" style={{padding:8,fontSize:25,  color:'#ea0017'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="Email" 
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({email:text})} />
            </View>
            <View style={styles.inputView}>
            <Icon name="ios-key-outline" style={{padding:8,fontSize:20,color:'#ea0017'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="********"
                underlineColorAndroid="transparent"
                secureTextEntry={true}
                onChangeText={(text) => this.setState({password:text})} />
            </View>
            {this.state.error != null ? <Text style={{color:'red',marginTop:3}}>{this.state.error}</Text>: null}
          </View>
          <View style={styles.btnView}>
            <TouchableOpacity style={!this.state.email || !this.state.password ? styles.disable : styles.btn} onPress={this.login} disabled={!this.state.email || !this.state.password} >
              <Text style={{color:'white',fontSize:16,fontWeight:'300',textAlign:'center'}}>Log in</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{width:'50%',alignItems:'center'}} onPress={() => Actions.forgetPassword()} ><Text style={{margin:15}}>Lost password ?</Text></TouchableOpacity>
            <TouchableOpacity style={styles.socialBtn} onPress={() => this._fblogin()} > 
              <Icon  type="FontAwesome" name="facebook" style={{color:'white',margin:5,marginRight:5,fontSize:20}} />
                <Text style={{color:'white',fontSize:16,padding:5,fontWeight:'300'}} >Connect with Facebook</Text>         
            </TouchableOpacity>
          </View>
        </View>
          <View style={{borderTopWidth:0.5,width:'100%',marginTop:20}}></View>
          <TouchableOpacity onPress={()=>Actions.signup()} style={{alignItems:'center',justifyContent:'center',width:'50%',marginTop:5,alignSelf:'center'}}>
            <Text style={{textAlign:'center',marginTop:10}}>Register Now</Text>
          </TouchableOpacity>
        </ScrollView>
        }
      </View>
    );}
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#ffffff', 
  },
  header: {
    backgroundColor:'#ea0017'
  },
  image:{
    alignItems:'center',
    margin:10
  },
  inputView:{
    flexDirection:'row',
    padding:5,
    borderWidth:1,
    width:'70%',
    height:50, 
    marginTop:10,
    borderRadius:5
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  disable:{
    backgroundColor: 'rgba(234, 0, 23, 0.5)',
    height:50,
    width:'70%',
    justifyContent:'center',
    borderRadius:5
  },
  btn:{
    backgroundColor:'#ea0017',
    height:50,
    width:'70%',
    justifyContent:'center',
    borderRadius:5
  },
  socialBtn:{
    width:220,
    flexDirection:'row',
    backgroundColor:'#445F95',
    padding:5,
    justifyContent:'center',
    borderRadius:5
  }
});
