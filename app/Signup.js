
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  AsyncStorage
} from 'react-native';
import { Icon} from 'native-base';
import Header from './components/Header1';
import { Actions } from 'react-native-router-flux';
import  FBSDK,{LoginManager,AccessToken} from 'react-native-fbsdk';
import Api from './controller/api';


export default class Signup extends Component {
  constructor(props){
    super(props);
    this.state={
      email:'',
      password:'',
      currentPage:1
    }
  }

  _fblogin(){

    LoginManager.logInWithReadPermissions(['public_profile','email']).then(
      function(result) {
        if (result.isCancelled) {
          alert('Login cancelled');
        } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              console.log(data)
              let token = data.accessToken;
              //console.log(token)

              fetch('https://graph.facebook.com/v3.0/me?fields=email,name&access_token=' + token)
              .then((resp) => resp.json())
              .then((result) => {
                console.log('facebook',result)

                var formdata = new FormData
                formdata.append('facebook_id',result.id)

                Api.post('app/api/checkActive',formdata)
                .then(res => { console.log(res)

                  if(res.user_model.address && res.user_model.phone_number && res.user_model.is_active == "Yes") 
                  {
                    AsyncStorage.multiRemove(['user_id','email','user_name','password','address','phone_number'])
                    AsyncStorage.setItem('user_id',res.user_model.user_id)
                    AsyncStorage.setItem('email',res.user_model.email)
                    AsyncStorage.setItem('user_name',res.user_model.user_name)
                    AsyncStorage.setItem('address',res.user_model.address)
                    AsyncStorage.setItem('phone_number',res.user_model.phone_number)
                    Actions.drawer()
                  }
                  else
                  {
                    Actions.add({user_name:result.name,id:result.id,email:result.email});
                  }
                })
              })
         
              })
        }
      },
      function(error) {
        alert('Login fail with error: ' + error);
      }
    );
  }
  render() {
    return (
      <View style={styles.container}>
       <Header/>
        {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>}       
      <ScrollView>
        {/* <View style={{marginTop:'10%'}}>
          <Indicator var={this.state.currentPage}/>
        </View> */}
        <View style={{marginTop:'20%',padding:10}}>
          <View style={{justifyContent:'center',alignItems:'center'}}>
          <TouchableOpacity onPress={()=>Actions.signup1()} 
                            style={{borderWidth:1,width:'70%',padding:10,alignItems:'center',borderRadius:5}}>
            <Text style={{fontSize:18}} >Create Account</Text>
          </TouchableOpacity>
           <Text style={{fontSize:18,marginTop:25}}>Or</Text>
          </View>
          <View style={styles.btnView}>
            <TouchableOpacity style={styles.btn} onPress={() => Actions.login()} >
              <Text style={{color:'white',fontSize:16,fontWeight:'300',textAlign:'center'}}>Sign in</Text>
            </TouchableOpacity>
            <Text style={{margin:10, color:'black', fontSize:18, fontWeight:'300'}}>Sign Up With </Text>
            <TouchableOpacity style={styles.socialBtn} onPress={()=>this._fblogin()}>
              <Icon type="FontAwesome" name="facebook" style={{color:'white',margin:5,marginRight:5,fontSize:20}} /><Text style={{color:'white',fontSize:15,padding:5,fontWeight:'300'}} >Connect with Facebook</Text>         
            </TouchableOpacity>
          </View> 
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
    
  },
  header: {
    backgroundColor:'#ea0017'
  },
  image:{
    alignItems:'center',
    margin:20
  },
  inputView:{
    flexDirection:'row',
    padding:10,
    borderWidth:0.5,
    width:'70%',
    marginTop:10,
    borderRadius:10
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  btn:{
    backgroundColor:'#ea0017',
    height:50,
    width:'70%',
    justifyContent:'center',
    borderRadius:5,
    marginTop:10
  },
  socialBtn:{
    width:220,
    flexDirection:'row',
    backgroundColor:'#445F95',
    padding:5,
    justifyContent:'center',
    borderRadius:5,
    marginBottom:15
  }
});
