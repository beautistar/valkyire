import React, { Component } from 'react';
import {Text, TouchableOpacity,StatusBar,Dimensions,Image,View,Platform} from 'react-native';
import { StackNavigator, DrawerNavigator, } from 'react-navigation';
import { Icon } from 'native-base';
import Main from './view/Main'
import Sidebar from './components/Sidebar';
import Clothes from './view/Clothes';
import Carpets from './view/Carpets';
import Blankets from './view/Blankets';
import Curtains from './view/Curtains';
import Cover from './view/Cover';
import Shoes from './view/Shoes';
import ConfirmOrder from './view/ConfirmOrder';
import Map from './components/Map';
import GetMyOrders from './view/GetMyOrders';
import MyAccount from './view/MyAccount';
import AboutUs from './view/AboutUs';

export const DrawerStack = DrawerNavigator({

Main : {screen:Main},
ConfirmOrder:{screen:ConfirmOrder},
MyAccount  : {screen:MyAccount},
Clothes: { screen:Clothes },
Carpets: { screen:Carpets },
Blankets: { screen:Blankets },
Curtains: { screen:Curtains },
Cover: { screen:Cover },
Shoes: { screen:Shoes },
Map : {screen:Map},
GetMyOrders : {screen:GetMyOrders},
AboutUs : {screen:AboutUs},
},
{
  contentComponent: props => <Sidebar {...props}  />,
  drawerWidth:Dimensions.get('window').width
}
)
const DrawerNavigation = StackNavigator({
  DrawerStack: { screen: DrawerStack }
}, {
  headerMode: 'float',
  navigationOptions: ({navigation}) => ({
    //headerStyle: {backgroundColor: '#1E2A52' #0954DB
    headerStyle: {backgroundColor: 'white',elevation: 0,shadowOpacity: 0},
    headerTitle:<View style={{width:'85%'}}><Image source={require('./images/logo.png')} style={Platform.OS=='ios'?{width:60,height:42,alignSelf:'center'}:{width:85,height:70,alignSelf:'center'}}/></View>,
    headerLeft: <TouchableOpacity style={{marginRight:35,width:'90%',padding:10}} onPress={() =>

      {if (navigation.state.index === 0) {
          navigation.navigate('DrawerOpen');
        } else {
          navigation.navigate('DrawerClose');
        }
      }
    }>
      <Icon name='menu'  style={{fontSize:30,color:'black' ,left:15}} /> 
    </TouchableOpacity>
  })
}

)
export const PrimaryNav = StackNavigator({
    drawerStack: { screen: DrawerNavigation }
  },{
    // Default config for all screens
     headerMode: 'none',
     initialRouteName: 'drawerStack',
  })

export default class index extends Component {
  render() {
    return (
      <PrimaryNav/>
    );
  }
}
