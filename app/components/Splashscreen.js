
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage
} from 'react-native';
//import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';
import AppHeader from '../components/Header';
import { Actions } from 'react-native-router-flux';

export default class Splashscreen extends Component {

  
  render() {
   
    return (
      <View style={styles.container}>
       <Image  source={require('../images/products/splashscreen.png')} style={{width:'101%',height:'100%'}}/>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  
 
});
