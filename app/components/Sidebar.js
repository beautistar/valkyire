
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  AsyncStorage,
  Share,
  Linking,
  Modal

} from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class Sidebar extends Component {
    constructor(props){
      super(props);
      this.state = {
        active:false
      }
    }


  logout(){
    AsyncStorage.clear()
    Actions.login()
  }

  _call(url){
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
       console.log('Can\'t handle url: ' + url);
      } else {
       return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  _email(url){
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
       console.log('Can\'t handle url: ' + url);
      } else {
       return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  _share(){
    Share.share(
      {
          message:'please install this application'        
      }).then(result => console.log(result)).catch(errorMsg => console.log(errorMsg));
  }

  _fb(){
    const url='fb://page/438948623186098'
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
       //console.log('Can\'t handle url: ' + url);
       return Linking.openURL('https://fb.com/438948623186098')
      } else {
       return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  
  }

  _insta(){
    const url='instagram://user?username=valkyrie_laundry'
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
       //console.log('Can\'t handle url: ' + url);
       return Linking.openURL('https://www.instagram.com/_u/valkyrie_laundry/')
      } else {
       return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  
  }

  render() {
    return (
      <View style={styles.container}>
      <StatusBar backgroundColor='#ea0017'  barStyle={Platform.OS=='android'?"light-content":null}/> 
        <View style={styles.btnview}>
            <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('Main')}> 
                    <Icon type='MaterialIcons' name='home'  style={{fontSize:30,color:'white'}} />
                    <Text style={styles.matirialicontext}>HOME</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('MyAccount')}> 
                     <Image source={require('../images/products/myaccount.png')} style={{width:20,height:25}}/>
                    <Text style={styles.icontext}>MY ACCOUNT</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('GetMyOrders')} > 
                    <Image source={require('../images/products/myorder.png')} style={{width:20,height:25}}/>
                    <Text style={styles.icontext}>MY ORDERS</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => this._share()} > 
                    <Icon type='MaterialIcons' name='share'  style={{fontSize:30,color:'white'}} />
                    <Text style={styles.matirialicontext}>SHARE APP</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => this.setState({active:!this.state.active}) } > 
                    <Icon type='MaterialIcons' name='phone-in-talk'  style={{fontSize:30,color:'white'}} />
                    <Text style={styles.matirialicontext}>CALL US</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop:40,marginLeft:25}} onPress={() => this.props.navigation.navigate('AboutUs')} > 
                        <Text style={{fontSize:15,fontWeight:'700',color:'white'}}>ABOUT US </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop:40,marginLeft:25}} onPress={() => this.logout()}> 
                        <Text style={{fontSize:15,fontWeight:'700',color:'white'}}>LOG OUT </Text>
            </TouchableOpacity>
        </View>
        <View style={{backgroundColor:'#ea0017',flex:0.2}}></View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.active}
          onRequestClose={() => this.setState({active:!this.state.active})}
        >
        <View style={{marginTop:20,flex:1,padding:20}}>
          <TouchableOpacity onPress={() => this.setState({active:!this.state.active})}>
          <Icon name = 'arrow-back'  />
          </TouchableOpacity>
          <View style={{justifyContent:'space-between',alignItems:'center',flexDirection:'row',margin:20}}>
              <View style={{justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity style={{margin:10,borderWidth:2,borderRadius:35,borderColor:'red',justifyContent:'center',alignItems:'center',height:70,width:70}} onPress={() => this._call('tel:+962789340608')}>
                  <Icon name='call' style={{fontSize:25,color:'red'}} />
                </TouchableOpacity >
                <Text style={{fontSize:12,color:'red'}}>CALL US</Text>
              </View>
              <View style={{justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity style={{margin:10,borderWidth:2,borderRadius:35,borderColor:'red',justifyContent:'center',alignItems:'center',height:70,width:70}} onPress={() => this._call('mailto:valkyrie.laundry@gmail.com')}>
                  <Icon type="Zocial"  name='email' style={{fontSize:22,color:'red'}} />
                </TouchableOpacity>
                <Text style={{fontSize:12,color:'red'}}>EMAIL US</Text>
              </View>
          </View>
          <Text style={{fontSize:20,color:'red',textAlign:'center',marginTop:'50%'}}>Valkyrie Social</Text>
          <View style={{justifyContent:'space-between',alignItems:'center',flexDirection:'row',margin:20}}>
              <View style={{justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity style={{margin:10,borderWidth:2,borderRadius:35,borderColor:'red',justifyContent:'center',alignItems:'center',height:70,width:70}} onPress={() => this._insta()}>
                  <Icon type="FontAwesome" name='instagram' style={{fontSize:25,color:'red'}} />
                </TouchableOpacity >
                <Text style={{fontSize:12,color:'red'}}>Instagram</Text>
              </View>
              <View style={{justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity style={{margin:10,borderWidth:2,borderRadius:35,borderColor:'red',justifyContent:'center',alignItems:'center',height:70,width:70}} onPress={() => this._fb()}>
                  <Icon type="FontAwesome" name='facebook-square' style={{fontSize:28,color:'red'}} />
                </TouchableOpacity >
                <Text style={{fontSize:12,color:'red'}}>Facebook</Text>
              </View>
          </View>
        </View>
        </Modal>
     </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'row',
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: 'black',
  },
  icontext: {
    left:18,
    fontSize: 15,
    textAlign: 'center',
    margin: 10,
    fontWeight:'700',
    color:'white',
  },
  matirialicontext: {
    left:10,
    fontSize: 15,
    textAlign: 'center',
    margin: 10,
    fontWeight:'700',
    color:'white',
  },
  btn:{
    left:20,
    alignItems:'center',
    flexDirection:'row',
    margin:5
},
  btnview:{
    flex:0.8,
    top:50,
  }
});
