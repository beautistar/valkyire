import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar
} from 'react-native';
import MapView from 'react-native-maps';
import { Header, Left, Body, Right, Button, Title, Icon } from 'native-base';
export default class MyApp extends Component {
  constructor(props){
    super(props);
    this.state={
      active:false
    }
  }
  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.1,
            longitudeDelta: 0.1,
          }}
        >
        </MapView>
        <View style={{ backgroundColor: 'white', zIndex: 1, padding: 2, alignSelf: 'center', flexDirection: 'row', alignItems: 'center', marginTop: 5, borderRadius: 3,height:Platform.OS == 'ios' ? 40 : null }}>
        <Icon name={this.state.active == false ? null : 'arrow-back'} style={{ marginLeft: 15, fontSize: 22 }} />
        <TextInput placeholder="Search" onFocus={()=>this.setState({active:true})} underlineColorAndroid="transparent" style={{ width: '60%', marginLeft: 10 }} />
          <Icon name={this.state.active == false ? null : 'close'} style={{fontSize:22}} />
          <Icon name='mic' style={{marginLeft:10,paddingRight:10,fontSize: 22 }} />
        </View>
        <View style={{top:Platform.OS == 'android' ? '55%' : '65%',alignSelf:'flex-end',margin:20}}>
          <Image source={require('../images/products/earth.png')} style={{height:25,width:25}} />
          <Image source={require('../images/products/pinlocation.png')} style={{height:25,width:25,marginTop:3}} />
        </View>
        <View style={{ bottom:0, position:'absolute', width: '100%' }}>
          <View style={{ flexDirection: 'row', backgroundColor: '#ea0017', width: '100%', height: 45, padding: 10 }}>
            <Image source={require('../images/products/pickuplocation.png')} style={{ marginLeft: 10, width: 18, height: 18 }} />
            <Text style={{ color: 'white', marginLeft: 10 }}>Pickup</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1, alignItems: 'center' }}>
              <Icon type="FontAwesome" name="search" style={{ color: 'white', fontSize: 12, marginRight: 3 }} />
              <Text style={{ color: 'white' }}>|</Text>
              <Icon name="md-star" style={{ color: 'white', fontSize: 16, marginLeft: 2 }} />
            </View>
          </View>
          <View>
            <View style={{ flexDirection: 'row', backgroundColor: 'white', width: '100%', height: 45, padding: 10 }}>
            <Icon type="Entypo" name="location-pin" style={{ color: '#E2E2E2', fontSize: 18, marginLeft:10 }} />
              <Text style={{ color: '#E2E2E2', marginLeft: 10 }}>DropOff</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1, alignItems: 'center' }}>
                <Icon type="FontAwesome" name="search" style={{ color: '#E2E2E2', fontSize: 12, marginRight: 3 }} />
                <Text style={{ color: 'white' }}>|</Text>
                <Icon name="md-star" style={{ color: '#E2E2E2', fontSize: 16, marginLeft: 2 }} />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    height: '90%',
    width: '100%',
    flex: 1
  },
});