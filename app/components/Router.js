import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar
} from 'react-native';
import {Router,Scene} from 'react-native-router-flux';
import Login from '../Login';
import ForgetPassword from '../ForgetPassword';
import ForgetPassword1 from '../ForgetPassword1';
import Signup from '../Signup';
import Signup1 from '../Signup1';
import HomeDrawer from '../index';
import ConfirmNumber from '../ConfirmNumber';
import Payments from '../view/Payments';
import Invoice from '../view/Invoice';
import Urgent from '../view/Urgent';
import Done from '../view/Done';
import Clothes from '../view/Clothes';
import Carpets from '../view/Carpets';
import Blankets from '../view/Blankets';
import Curtains from '../view/Curtains';
import Cover from '../view/Cover';
import Shoes from '../view/Shoes';
import ConfirmOrder from '../view/ConfirmOrder';
import AddressPhone from '../AddressPhone';
import Distinguish from '../view/DistinguishUs';
import PrivacyPolicy from '../view/PrivacyPolicy';
import TermsAndConditions from '../view/TermsAndConditions';


export default class Route extends Component{
  render() {
    return (
      <Router >
          <Scene key="root" hideNavBar={true} >
                <Scene key="login" component={Login} initial />
                <Scene key="signup" component={Signup} />
                <Scene key="signup1" component={Signup1} />
                <Scene key="confirmNumber" component={ConfirmNumber}  />
                <Scene key="drawer" component={HomeDrawer} panHandlers={null} />
                <Scene key="payments" component={Payments}  /> 
                <Scene key="invoice" component={Invoice}  />
                <Scene key="urgent" component={Urgent} />
                <Scene key="done" component={Done} />
                <Scene key="forgetPassword" component={ForgetPassword}  />
                <Scene key="forgetPassword1" component={ForgetPassword1}  />
                <Scene key="clothes" component={Clothes} />
                <Scene key="carpets" component={Carpets} />
                <Scene key="curtains" component={Curtains} />
                <Scene key="blankets" component={Blankets} />
                <Scene key="cover" component={Cover} />
                <Scene key="shoes" component={Shoes} />
                <Scene key="confirmOrder" component={ConfirmOrder} />
                <Scene key="add" component={AddressPhone}  />
                <Scene key="distinguish" component={Distinguish}  />
                <Scene key="privacyPolicy" component={PrivacyPolicy}  />
                <Scene key="termsAndConditions" component={TermsAndConditions}  />
          </Scene>
      </Router>
      );
  }
}
