
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state={
      currentPage:this.props.var
    }
  }
  render() {
    return (
        <View style={{justifyContent:'center',flexDirection:'row',flex:1,alignItems:'center'}}>
            <View style={{height:30,width:30,borderRadius:50,borderWidth:0.5,justifyContent:'center',backgroundColor:this.state.currentPage >= 1 ? '#ea0017' : null}}>
                <Text style={{fontFamily: Platform.OS == 'android' ? 'HelveticaNeueIt' : null,fontWeight:'400',textAlign:'center',color:this.state.currentPage >= 1 ? 'white' : null}}>01</Text>
            </View>
            <View style={{width:30,borderBottomWidth:1,borderBottomColor:this.state.currentPage >= 2 ? '#ea0017' : null}}></View>
            <View style={{height:30,width:30,borderRadius:50,borderWidth:0.5,justifyContent:'center',backgroundColor:this.state.currentPage >= 2 ? '#ea0017' : null}}>
                <Text style={{fontFamily:Platform.OS == 'android' ? 'HelveticaNeueIt' : null,fontWeight:'400',textAlign:'center',color:this.state.currentPage >= 2 ? 'white' : null}}>02</Text>
            </View>
            <View style={{width:30,borderBottomWidth:1,borderBottomColor:this.state.currentPage >= 3 ? '#ea0017' : null}}></View>
            <View style={{height:30,width:30,borderRadius:50,borderWidth:0.5,justifyContent:'center',backgroundColor:this.state.currentPage >= 3 ? '#ea0017' : null}}>
                <Text style={{fontFamily:Platform.OS == 'android' ? 'HelveticaNeueIt' : null,fontWeight:'400',textAlign:'center',color:this.state.currentPage >= 3 ? 'white' : null}}>03</Text>
            </View>
            <View style={{width:30,borderBottomWidth:1,borderBottomColor:this.state.currentPage >= 4 ? '#ea0017' : null}}></View>
            <View style={{height:30,width:30,borderRadius:50,borderWidth:0.5,justifyContent:'center',backgroundColor:this.state.currentPage >= 4 ? '#ea0017' : null}}>
                <Text style={{fontFamily:Platform.OS == 'android' ? 'HelveticaNeueIt' : null,fontWeight:'400',textAlign:'center',color:this.state.currentPage >= 4 ? 'white' : null}}>04</Text>
            </View>
            <View style={{width:30,borderBottomWidth:1,borderBottomColor:this.state.currentPage >= 5 ? '#ea0017' : null}}></View>
            <View style={{height:30,width:30,borderRadius:50,borderWidth:0.5,justifyContent:'center',backgroundColor:this.state.currentPage >= 5 ? '#ea0017' : null}}>
                <Text style={{fontFamily:Platform.OS == 'android' ? 'HelveticaNeueIt' : null,fontWeight:'400',textAlign:'center',color:this.state.currentPage >= 5 ? 'white' : null}}>05</Text>
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor:'red'
  },
  image:{
    alignItems:'center',
    margin:20
  },
  inputView:{
    flexDirection:'row',
    padding:10,
    borderWidth:0.5,
    width:'70%',
    marginTop:10,
    borderRadius:10
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  btn:{
    backgroundColor:'red',
    height:50,
    width:'70%',
    justifyContent:'center',
    borderRadius:5
  }
});
