
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView
} from 'react-native';
//import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';
import AppHeader from '../components/Header';
import { Actions } from 'react-native-router-flux';

export default class PrivacyPolicy extends Component {

  constructor(props){
    super(props);
    this.state={
      data:this.props.privacy
    }

  }
  render() {
    return (
      <View style={styles.container}>
            <AppHeader/>
            {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
                                        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 

             <View style={{backgroundColor:'white',alignItems:'center',width:'100%', flex:1,marginTop:5}}>
                <View style={{marginTop:15}}>
                    <Text style={{fontSize:25,fontWeight:Platform.OS == 'android' ? '500' : '800',color:'#ea0017'}}>Privacy Policy</Text>
                </View>
                <View style={{marginTop:10}}>
                  <Text>{this.state.data}</Text>
                </View>
             </View>

        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  header: {
    backgroundColor:'#ffffff'
  },
  title:{
    color:'white',
    fontWeight:'700',
    fontSize:20
  },
  box:{
    margin:5,
    width:180,
    height:250,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    textAlign:'center',
    top:120,
    color:'white',
    fontWeight:'500',
    fontSize:15
  },
  activeBox:{
    margin:5,
    width:180,
    height:250,
    backgroundColor:'white'
  },
  activeBoxText:{
    textAlign:'center',
    top:120,
    color:'#ea0017',
    fontWeight:'500',
    fontSize:15
  },
  headerImage : {
    width:90,
    height:60,
    alignSelf:'center'
  },
  view1 :{
    justifyContent:'space-between',
    alignItems:'center',
  },
  view2:{
    backgroundColor:'#000000',
    width:'85%',
    flexDirection:'row',
    padding:35,
    justifyContent:'center'
  },
  view3:{
    backgroundColor:'#ffffff',
    width:'85%',
    flexDirection:'row',
    marginTop:20,
    padding:35,
    justifyContent:'center'

  },
  view4:{
    backgroundColor:'#000000',
    height:50,
    justifyContent:'center'
  },
  text1:{
    color:'white',
    fontSize:20,
    fontWeight:'500',
    textAlign:'center'
    
  },
  text2:{
    color:'red',
    fontSize:20,
    fontWeight:'500',
    textAlign:'center'
    
  },
  text3:{
    color:'white',
    fontWeight:'700',
    fontSize:22,
    textAlign:'center'
  }
 
});
