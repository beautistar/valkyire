
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
  FlatList,
  StatusBar,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Api from '../controller/api';
import Header from '../components/Header';


var total_order=[],orders= [],list=[];
var total_item=0,total_price=0,count=0;
export default class ConfirmOrder extends Component {

  constructor(props){
    super(props);
    this.state={
      orders:'',
      totalprice:0,

    }
  }

  componentDidMount(){
    var c=0;
    total_order = []
    orders = []
    list = []
    console.log('___TOTAL____',total_order)
    console.log('____ORDERS___',orders)
    AsyncStorage.getItem('totalprice')
    .then((price) => {console.log('---------price',price), total_price = price
        // if(price)
        // {


      AsyncStorage.getItem('totalitem')
      .then((item) => total_item = item)
      //  AsyncStorage.getItem('user_id')
      //  .then((id) => {this.setState({userID:id})})

      AsyncStorage.getItem('orderList')
      .then((items) => {console.log('ClothesOrder',JSON.parse(items))
                        if(JSON.parse(items) != null)
                        {
                          total_order[c] = JSON.parse(items)
                          c++;
                          //console.log('ClothesOrder',total_order)
                        }
                        })

      AsyncStorage.getItem('blanketList')
      .then((items) => {console.log('Blanketorder',JSON.parse(items))
                        if(JSON.parse(items) != null)
                        {
                        total_order[c] = JSON.parse(items)
                        c++;
                        //console.log('TOTAL',total_order)
                        }
                        })

      AsyncStorage.getItem('curtainsList')
      .then((items) => {console.log('Curtainsorder',JSON.parse(items))
                        if(JSON.parse(items) != null)
                        {
                          total_order[c] = JSON.parse(items)
                          c++;
                          //console.log('TOTAL',total_order)
                        }
                        })
      AsyncStorage.getItem('carpetList')
        .then((items) => {console.log('Carpetsorder',JSON.parse(items))
                        if(JSON.parse(items) != null)
                        {
                          total_order[c] = JSON.parse(items)
                          c++;
                          //console.log('TOTAL',total_order)
                        }
                          })
      AsyncStorage.getItem('coverList')
      .then((items) => {console.log('Coversorder',JSON.parse(items))
                        if(JSON.parse(items) != null)
                        {
                          total_order[c] = JSON.parse(items)
                          c++;
                          //console.log('TOTAL',total_order)
                        }
                        })

      AsyncStorage.getItem('shoesList')
      .then((items) => {
                        console.log('Shoesorder',JSON.parse(items))
                        if(JSON.parse(items) != null)
                        {
                          total_order[c] = JSON.parse(items)
                          c++;
                          console.log('TOTAL',total_order)
                        }
                        
                        count=0
                        for(i=0;i < total_order.length;i++)
                        {
                          if(total_order[i] != null)
                          {
                          // console.log("........total_order............",total_order[i])
                              for(j=0;j<total_order[i].length;j++)
                            {
                            // console.log(total_order[i][j])
                            if(orders.indexOf(total_order[i].id)===-1){
                                orders[count]=total_order[i][j];
                                count++;
                            }
                            }
                          }

                        }
                        console.log('--------------order----',orders)
                        count = 0;
                        
                        if(orders != null)
                        {
                          for(i=0;i<orders.length;i++)
                          {
                            if(orders[i].qty > 0)
                          {
                                // total_item = total_item + orders[i].qty
                                // total_price = total_price + Number(orders[i].price)
                          
                                  if(list.indexOf(orders[i].id)===-1){
                                  
                                      list[count]=orders[i]
                                      count++
                                    }
              
                                }
                                
                          }
                          console.log('mainlist',list)

                        }
                        
                         var list1 = []
                         for(i=0;i<count;i++)
                         {
                          if(list1.indexOf(list[i].id)===-1){
                           list1[i] = list[i]
                          }
                         }
                        console.log("list",list1)
                        this.setState({orders:list1})
                        })
        // }       
    })
  } 


  _confirmOrder()
  {
    if(this.state.orders != '')
    {
      Actions.urgent({order:this.state.orders,item:total_item,price:total_price})
    }
    else
    {
      alert('You must add item before you confirm order')
    }
  }

  render() {
       
    return (
      <View style={styles.container}>
      <Header/>
      {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
      <ScrollView>
        <View style={{marginTop:15,justifyContent:'center',alignItems:'center'}}> 
          <Text style={styles.title}>YOUR ITEMS</Text>
        </View>
        <FlatList 
          data={this.state.orders}
          renderItem={({item , index}) => 
          <View style={{borderBottomWidth:0.5,flexDirection:'row',justifyContent:'space-between',backgroundColor:'white',padding:5,alignItems:'center',paddingLeft:15,paddingRight:15}}>
            <Image source = {{uri:item.image}} style={{height:50,width:50}} />
            <Text>{item.name}</Text>
            <Text>{item.price} JD</Text>
            <Text>{item.qty}</Text>
          </View> 
        }
        keyExtractor={(item,index) => item.id}
        />
      </ScrollView>
      <TouchableOpacity style={styles.btn} onPress={() => this._confirmOrder() }>
      <Text style={{color:'white',fontSize:20}}>CONFIRM YOUR ORDER</Text>
      </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:15
   
  },
  box:{
    backgroundColor:'#fa6c70',
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  btn:{
    marginTop:1.5,
    flexDirection:'row', 
    width:'100%',
    height:50,
    backgroundColor:'black',
    borderWidth:2,
    borderColor:'white',
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center'
   },
   view1:{
    flexDirection:'row',
    margin:10,
    marginTop:20,
    justifyContent:'space-between'
   },
   view:{
    flexDirection:'row',
    margin:10,
    marginTop:-5,
    justifyContent:'space-between'
   }
 
});
