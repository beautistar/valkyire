
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage
} from 'react-native';
//import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';
import AppHeader from '../components/Header';
import { Actions } from 'react-native-router-flux';
import Api from '../controller/api';

var lat,long;
export default class Payments extends Component {
  constructor(props){
    super(props);
    this.state={
      cash:false,
      visa:true,
      userID:'',
      userName:'',
      address:'',
      item:0,
      price:0,
      code:this.props.code
    }
  }
  componentWillMount(){
    let totalItems=this.props.item
    let totalPrice= this.props.price
    this.setState({item:totalItems,price:totalPrice})
    console.log('CODE',this.state.code)
  }

  componentDidMount(){

    navigator.geolocation.getCurrentPosition((position) => {
      lat = parseFloat(position.coords.latitude)
      long = parseFloat(position.coords.longitude)
     console.log('lat',lat)
     console.log('long',long)
    },
    (error) => console.log(error),
    {enableHighAccuracy: true,timeout:20000}
  ) 

    AsyncStorage.getItem('user_id')
   .then((id) => {this.setState({userID:id})})
  
   AsyncStorage.getItem('user_name')
   .then((name) => {this.setState({userName:name})})

   AsyncStorage.getItem('address')
   .then((add) => {this.setState({address:add})})

    
    
  }

  makeOrder(){
     
    let userData={'user_id':this.state.userID,'total_itmes':this.state.item, 'total_cost':this.state.price,'user_name':this.state.userName,'invoice_number':null,'invoice_date':new Date().toLocaleDateString(),}

    var invoiceNumber = this.props.invoice

    var day,month,year;
    var date= (new Date()).toISOString().split("T")[0];
    console.log(date)
   

    var formData = new FormData()
    formData.append('user_id',Number(this.state.userID));
    formData.append('total_items',this.state.item);
    formData.append('total_cost',this.state.price);
    formData.append('user_name',this.state.userName);
    formData.append('invoice_number',invoiceNumber);
    formData.append('invoice_date',date);
    formData.append('address',this.state.address);
    formData.append('latitude',lat);
    formData.append('longitude',long)

    console.log(formData)

      Api.post('app/api/makeOrder',formData)
      .then(result => {
        if(result.message == 'Success.')
        {
           AsyncStorage.multiRemove(
            [ 'totalitem','totalprice',
              'blanket_item','blanket_price',
              'carpet_item','carpet_price',
              'curtain_item','curtain_price',
              'shoes_item','shoes_price',
              'cover_item','cover_price',
              'men_qty',
              'women_qty',
              'kids_qty',
              'blanket_qty',
              'carpet_qty',
              'curtain_qty',
              'shoes_qty',
              'cover_qty',
              'orderList',
              'blanketList',
              'carpetList',
              'curtainsList',
              'shoesList',
              'coverList'
            ])

            AsyncStorage.getItem('orderList')
            .then(res => console.log('----------RES-------',res))

            AsyncStorage.setItem('clearorder','true')
            Actions.done()
        }
      })
        
  }


  render() {
    return (
      <View style={styles.container}>
          <AppHeader/>
          {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
          <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
        <ScrollView>
        <View style={{marginTop:'40%'}}>
            <View style={styles.view1} >
                <TouchableOpacity onPress={() => this.setState({cash:true,visa:false})} style={this.state.cash == false ? styles.view2 : styles.view3}>
                      <Image source={this.state.cash == false ? require('../images/cash.png') : require('../images/cash1.png')} style={styles.icon}/>
                      <Text style={this.state.cash == false ? styles.text1 : styles.text2}>CASH ON DELIVERY</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({cash:false,visa:true})} style={this.state.visa == true ? styles.view3 : styles.view2}>
                      <Image source={this.state.visa == true ? require('../images/visa1.png') : require('../images/visa.png')} style={styles.icon}/>
                      <Text style={this.state.visa == true ? styles.text2 : styles.text1}>VISA CARD</Text>
                </TouchableOpacity>
            </View>
        </View>
        </ScrollView>
        <TouchableOpacity style={styles.view4} onPress={() => this.makeOrder()} >
            <Text style={styles.text3}>DONE</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
    justifyContent:'space-between'
  },
  header: {
    backgroundColor:'#ffffff'
  },
  title:{
    color:'white',
    fontWeight:'700',
    fontSize:20
  },
  box:{
    margin:5,
    width:180,
    height:250,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    textAlign:'center',
    top:120,
    color:'white',
    fontWeight:'500',
    fontSize:15
  },
  activeBox:{
    margin:5,
    width:180,
    height:250,
    backgroundColor:'white'
  },
  activeBoxText:{
    textAlign:'center',
    top:120,
    color:'#ea0017',
    fontWeight:'500',
    fontSize:15
  },
  headerImage : {
    width:90,
    height:60,
    alignSelf:'center'
  },
  view1 :{
    justifyContent:'space-between',
    alignItems:'center'
  },
  view2:{
    backgroundColor:'#000000',
    width:'85%',
    flexDirection:'row',
    marginTop:20

  },
  view3:{
    backgroundColor:'#ffffff',
    width:'85%',
    flexDirection:'row',
    marginTop:20
  },
  view4:{
    backgroundColor:'#000000',
    height:50,
    justifyContent:'center'
  },
  icon:{
    height:40,
    width:40,
    margin:20
  },
  text1:{
    color:'white',
    marginLeft:10,
    fontSize:20,
    fontWeight:'500',
    alignSelf:'center'
  },
  text2:{
    color:'red',
    marginLeft:10,
    fontSize:20,
    fontWeight:'500',
    alignSelf:'center'
  },
  text3:{
    color:'white',
    fontWeight:'700',
    fontSize:22,
    textAlign:'center'
  }
 
});
