import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    AsyncStorage
} from 'react-native';

var service;
var name = [] , name1 = [];

export default class Kids extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item:0,
            price:0,
            iron:false,
            washing:false,
            q:this.props.qty

        }
    }
    wash(id){
        items = this.state.q
            for(i=0;i<items.length;i++)
            {
              if(items[i].id == id)
              {
                items[i].wash = !items[i].wash
              }
            }
          this.setState({q:items})
          console.log(this.state.q)
          AsyncStorage.setItem('kids_qty',JSON.stringify(this.state.q))
          //this.props.total(this.state.item ,this.state.price)
          console.log(this.state.q)
       }
      
       iron(id){
        items = this.state.q
            for(i=0;i<items.length;i++)
            {
              if(items[i].id == id)
              {
                items[i].iron = !items[i].iron
              }
            }
          this.setState({q:items})
          console.log(this.state.q)
          AsyncStorage.setItem('kids_qty',JSON.stringify(this.state.q))
          //this.props.total(this.state.item ,this.state.price)
          console.log(this.state.q)
       }
      

    componentDidMount(){
        this.setState({item:this.props.total_item , price: this.props.total_price})
        AsyncStorage.getItem('kids_qty')
        .then(result => {
            if(result != null )
            {
              this.setState({q:JSON.parse(result)})
            }
            else
            {
              this.setState({q:this.props.qty})
            }
    
        })
      }

      total(id,price , remove){
        var totalitem=0 , totalprice=0
        totalitem = this.props.total_item , totalprice = this.props.total_price
       
        if(remove == false)
        {
            totalprice = totalprice + Number(price) 
            totalitem = totalitem + 1
        }
        else
        {
            totalprice = totalprice - Number(price) 
            totalitem = totalitem - 1
        }
    
        this.props.total(id,totalitem,totalprice,remove)
      }      


    add(id, index, price){
       // this.setState({ item: this.state.item + 1 , price : this.state.price + Number(price) })
        //this.props.total(this.state.item,this.state.price)
        items = this.state.q
        for(i=0;i<items.length;i++)
        {
          if(items[i].id == id)
          {
            items[i].qty = items[i].qty + 1
          }
        }
        this.setState({q:items})
        AsyncStorage.setItem('kids_qty',JSON.stringify(this.state.q) )
        console.log(this.state.q)
         this.total(id,price, false)
        }
    
      remove(id, index, price ){
          if(this.state.q[index].qty != 0)
          {
            //   this.setState({ item: this.state.item - 1 , price : this.state.price - Number(price) })
            //   this.props.total(this.state.item,this.state.price)

              items = this.state.q
              for(i=0;i<items.length;i++)
              {
                if(items[i].id == id)
                {
                  items[i].qty = items[i].qty - 1
                }
              }
              this.setState({q:items})
              AsyncStorage.setItem('kids_qty',JSON.stringify(this.state.q) )

              console.log(this.state.q)
              this.total(id,price,true)

          }
          
      }
    
    render() {

        let service = this.props.service;
        
      //  this.props.total(this.state.item,this.state.price)
        return (
            <View style={styles.container}>

                <View  style={{ flexDirection: 'row', margin: 10, marginTop: 20, marginBottom: 10,flexWrap:'wrap',justifyContent:'center' }}>
                {service.map((item,index) => (

                <View key={index} style={styles.box}>
                    <Text style={styles.boxText}>{item.service_name}</Text>
                    <Image source={{ uri: item.photo_url }} style={{ width: 90, height: 100, alignSelf: 'center', marginTop: 10 }} />
                    <Text style={styles.h1}>{item.price} JOD</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                        <TouchableOpacity style={styles.btncricle} onPress={() => this.remove(item.service_id,index,item.price)}>
                            <Text style={styles.btncricleText}>-</Text>
                        </TouchableOpacity>
                        <Text style={styles.h2}>{this.state.q[index].qty + " Pec"}</Text>
                        <TouchableOpacity style={styles.btncricle} onPress={() => this.add( item.service_id,index,item.price)}>
                            <Text style={styles.btncricleText}>+</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                        <TouchableOpacity onPress={() => {this.setState({washing:!this.state.washing}),this.props.washing(item.service_id),this.wash(item.service_id)}} style={{ width: 30, height: 30, borderRadius: 25, justifyContent: 'center', alignSelf: 'flex-start', margin: 10, backgroundColor: '#f98a8b' }} >
                            <Image source={this.state.q[index].wash == true ? require('../images/products/washing.png') : require('../images/products/washing1.png')} style={{ width: 15, height: 20, alignSelf: 'center' }} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {this.setState({iron:!this.state.iron}),this.props.ironing(item.service_id),this.iron(item.service_id)}} style={{ width: 30, height: 30, borderRadius: 25, justifyContent: 'center', alignSelf: 'flex-end', margin: 10, backgroundColor: '#f98a8b' }} >
                            <Image source={this.state.q[index].iron == true ? require('../images/products/ironing.png') : require('../images/products/ironing1.png') } style={{ width: 25, height: 23, alignSelf: 'center' }} />
                        </TouchableOpacity>
                    </View>
                </View>
                ))}
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ea0017',
    },
    title: {
        color: 'white',
        fontWeight: '700',
        fontSize: 20
    },
    box: {
        margin: 2,
        width: '45%',
        
        borderWidth: 1,
        borderColor: 'white'
    },
    boxText: {
        marginTop: 10,
        textAlign: 'center',
        color: 'white',
        fontSize: 15
    },
    activeBox: {
        margin: 2,
        width: '45%',
        height: 256,
        backgroundColor: 'white'
    },
    activeBoxText: {
        marginTop: 10,
        textAlign: 'center',
        color: '#ea0017',
        fontSize: 15
    },
    btn: {
        width: 50,
        height: 50,
    },
    btnText: {

    },
    tab: {
        backgroundColor: 'white',
    },
    activeTab: {
        backgroundColor: 'black',
        color: 'white',
    },
    tabText: {
        color: 'black',
        //fontWeight:'bold'
    },
    image: {
        margin: 10,
        width: '70%',
        height: '50%',
        alignSelf: 'center'
    },
    h1: {
        marginTop: 10,
        textAlign: 'center',
        color: 'white',
        fontSize: 10,
    },
    h2: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: 'center',
        color: 'white',
        fontSize: 10,
    },
    btncricle: {
        borderRadius: 10,
        borderWidth: 1,
        width: 20,
        height: 20,
        borderColor: 'white',
        alignItems: 'center'
    },
    btncricleText: {
        fontWeight: '700',
        color: 'white',
        fontSize: 15,
        marginTop: -1.5,
    },
    activeH1: {
        marginTop: 10,
        textAlign: 'center',
        color: '#ea0017',
        fontSize: 10,
    },
    activeH2: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: 'center',
        color: '#ea0017',
        fontSize: 10,
    },
    activeBtncricle: {
        borderRadius: 10,
        borderWidth: 1,
        width: 20,
        height: 20,
        borderColor: '#ea0017',
        alignItems: 'center'
    },
    activeBtncricleText: {
        color: '#ea0017',
        fontSize: 15,
        fontWeight: '700',
        marginTop: -1.5,
    }
});
