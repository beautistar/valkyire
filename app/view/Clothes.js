import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  ScrollView,
  AsyncStorage,
  BackHandler,
  StatusBar
} from 'react-native';
import Header from '../components/Header';
import Men from '../view/Men';
import Women from '../view/Women';
import Kids from '../view/Kids';
import Curtains from '../view/Curtains';
import { Actions } from 'react-native-router-flux';

var orderList = [];
export default class Clothes extends Component {
  constructor(props){
    super(props);
    this.state={
      activeTab:'men',
      totalItems:0,
      totalPrice:0,
      menItems:0,
      menPrice:0,
      kidsItems:0,
      kidsPrice:0,
    }
    
    this.total = this.total.bind(this)
  }

  _wash(id){
    AsyncStorage.getItem('orderList')
    .then((resp) => {
            
              orderList=JSON.parse(resp)
            
            for(i=0;i<orderList.length;i++)
            {
              if(orderList[i].id == id)
              {
                orderList[i].wash = !orderList[i].wash
              }
            }
            AsyncStorage.setItem('orderList',JSON.stringify(orderList))
            console.log('WASHING',orderList)
    })
  }
  
  _iron(id){
    AsyncStorage.getItem('orderList')
    .then((resp) => {
            
              orderList=JSON.parse(resp)
            
            for(i=0;i<orderList.length;i++)
            {
              if(orderList[i].id == id)
              {
                orderList[i].iron = !orderList[i].iron
              }
            }
            AsyncStorage.setItem('orderList',JSON.stringify(orderList))
            console.log('WASHING',orderList)
    })
  }



  total(id,items,price,remove){
    console.log('item',items,'price',price)

   // this.setState({totalItems:this.state.totalItems + items ,totalPrice:this.state.totalPrice + price})

    if(this.state.totalItems != items || this.state.totalPrice != price)
    {
      this.setState({totalItems:items,totalPrice:price}, () => {console.log('callback')})
    }
    console.log('ITEM',this.state.totalItems,'PRICE',this.state.totalPrice)
      AsyncStorage.setItem('totalitem',items.toString())
      AsyncStorage.setItem('totalprice',price.toString())

      AsyncStorage.getItem('orderList')
      .then(result => {
        
          orderList = JSON.parse(result)
          orderList.map((item,index) => {
            if(item.id == id)
            {
              console.log('remove',remove)
              if(remove == false)
              {
                orderList[index].qty = orderList[index].qty + 1
              }
              else
              {
                orderList[index].qty = orderList[index].qty - 1
              }
              
            } 
          })
            console.log('data',orderList)
            AsyncStorage.setItem('orderList',JSON.stringify(orderList))
        
      
      })
  }

  componentWillMount(){
    AsyncStorage.getItem('totalitem')
    .then(result => {
        console.log('items',result)
        this.setState({totalItems:Number(result)})
      })
      AsyncStorage.getItem('totalprice')
    .then(result => {
        console.log('price',result)
        this.setState({totalPrice:Number(result)})
      })


  }

  render() {
      console.log('render')
      tab=this.state.activeTab
     
      let service = this.props.service
      console.log(service)
      var woman = [] , men = [] , kids = [];
     
      var woman_items = [] , men_items = [] , kids_items = [];
     
      for(i=0;i<service.length;i++)
      {
       orderList[i] = {price:service[i].price,name:service[i].service_name,id:service[i].service_id,image:service[i].photo_url,qty:0,wash:false,iron:false} 
       AsyncStorage.getItem('orderList')
       .then((result) => {
            if(result == null)
            {
              AsyncStorage.setItem('orderList',JSON.stringify(orderList))
            }
            
       } )

       if(service[i].category == 'Women')
       {
         woman.push(service[i])
       }
       if(service[i].category == 'Men')
       {
         men.push(service[i])
       }
       if(service[i].category == 'Kids')
       {
         kids.push(service[i])
       }
      }
      console.log('orderList',orderList)
      woman.map((item,index) => 
            {
              woman_items[index] = {id:item.service_id, qty:0, wash:false, iron:false} 
            }
      )

      men.map((item,index) => 
            {
              men_items[index] = {id:item.service_id, qty:0, wash:false, iron:false}  
            }
      )

      kids.map((item,index) => 
            {
              kids_items[index] = {id:item.service_id, qty:0, wash:false, iron:false}  
            }
      )
      // console.log(woman)
      // console.log(men)
      // console.log(kids)
    return (
      <View style={styles.container}>
      <Header/>
      {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
     <View>
      <View style={{flexDirection:'row',backgroundColor:'white',justifyContent:'center'}}>
        <TouchableOpacity style={{backgroundColor:tab=='men'?'black':'white',width:'33%',height:50}} onPress={()=>this.setState({activeTab:'men'})}>
          <View style={{justifyContent:'center',margin:10}}> 
            <Image source={tab=='men'?require('../images/products/t-shirts.png'):require('../images/products/t-shirts2.png')} style={{width:20,height:24,alignSelf:'center'}}/>
            <Text style={{textAlign:'center',fontWeight:'500',color:tab=='men'?'white':'black'}}>Men</Text>
         </View>
        </TouchableOpacity>
        <TouchableOpacity style={{backgroundColor:tab=='women'?'black':'white',width:'33%',height:50}} onPress={()=>this.setState({activeTab:'women'})}>
          <View style={{justifyContent:'center',margin:10}}> 
            <Image source={tab=='women'?require('../images/products/clothes2.png'):require('../images/products/clothes3.png')} style={{width:15,height:25,alignSelf:'center'}}/>
            <Text style={{textAlign:'center',fontWeight:'500',color:tab=='women'?'white':'black'}}>Women</Text>
         </View>
        </TouchableOpacity>
        <TouchableOpacity style={{backgroundColor:tab=='kids'?'black':'white',width:'33%',height:50}} onPress={()=>this.setState({activeTab:'kids'})}>
          <View style={{justifyContent:'center',margin:10}}> 
            <Image source={tab=='kids'?require('../images/products/kids.png'):require('../images/products/kids2.png')} style={{width:18,height:22,alignSelf:'center'}}/>
            <Text style={{textAlign:'center',fontWeight:'500',color:tab=='kids'?'white':'black'}}>Kids</Text>
         </View>
        </TouchableOpacity>
        {/* <TouchableOpacity style={{backgroundColor:tab=='carpet'?'black':'white',width:'20%',height:50}} onPress={()=>this.setState({activeTab:'carpet'})}>
          <View style={{justifyContent:'center',margin:10}}> 
            <Image source={tab=='carpet'?require('../images/products/carpet.png'):require('../images/products/carpet3.png')} style={{width:24,height:24,alignSelf:'center'}}/>
            <Text style={{textAlign:'center',fontWeight:'500',color:tab=='carpet'?'white':'black'}}>Carpet</Text>
         </View>
        </TouchableOpacity>
        <TouchableOpacity style={{backgroundColor:tab=='cover'?'black':'white',width:'20%',height:50}} onPress={()=>this.setState({activeTab:'cover'})}>
          <View style={{justifyContent:'center',margin:10}}> 
            <Image source={tab=='cover'?require('../images/products/cover.png'):require('../images/products/cover2.png')} style={{width:22,height:24,alignSelf:'center'}}/>
            <Text style={{textAlign:'center',fontWeight:'500',color:tab=='cover'?'white':'black'}}>Cover</Text>
         </View>
        </TouchableOpacity> */}
      </View>
      </View>
      <ScrollView>
      <View style={{marginBottom:10}}>
        {
          tab=='men'?
             <Men service = {men}  washing={this._wash}  ironing={this._iron}  qty= {men_items}  total={this.total} total_item = {this.state.totalItems} total_price = {this.state.totalPrice} />
          :
          tab=='women'?
              <Women service = {woman} washing={this._wash}  ironing={this._iron} qty = {woman_items} total={this.total} total_item = {this.state.totalItems} total_price = {this.state.totalPrice}/>
          :
          tab=='kids'?
              <Kids service = {kids} washing={this._wash}  ironing={this._iron} qty = {kids_items} total={this.total} total_item = {this.state.totalItems} total_price = {this.state.totalPrice}/>
          :
          tab=='carpet'?
              <View></View>
          :
          tab=='cover'?
              <View></View>
          :
          null
        }
      
       </View>
       </ScrollView>
       <View style={{flexDirection:'row',width:'86%',alignSelf:'center'}}>
         <View style={{backgroundColor:'black',height:50,width:'50%',justifyContent:'center',alignItems:'center'}}>
         <Text style={styles.text}>TOTAL ITEMS</Text>
         <Text style={styles.text}> {this.state.totalItems} items</Text>
         </View>
         <View style={{backgroundColor:'white',width:'50%',justifyContent:'center',alignItems:'center'}}>
         <Text style={styles.text2}>TOTAL COST</Text>
         <Text style={styles.text2}> {this.state.totalPrice} JOD</Text>
         </View>
       </View>
       <TouchableOpacity style={styles.btn} onPress={()=> this.state.totalItems == 0 ? alert('You must add item before you confirm order') : Actions.confirmOrder({totalItems:this.state.totalItems,totalPrice:this.state.totalPrice})}>
       <Text style={{color:'white',fontSize:20}}>CONFIRM YOUR ORDER</Text>
       </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   // justifyContent: 'center',
   // alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontWeight:'700',
    fontSize:20
  },
 
  activeTab:{
    backgroundColor:'black',
    color:'red',
   } ,
   tabText:{
    color:'white',
    //fontWeight:'bold'
   },
   tab:{
    justifyContent:'center',
    margin:10
   },
   text:{
    color:'white',
    fontWeight:'500',
    color:'white',
   },
   text2:{
    color:'white',
    fontWeight:'500',
    color:'#ea0017',
   },
   btn:{
    marginTop:1.5,
    flexDirection:'row', 
    width:'86%',
    height:50,
    borderWidth:2,
    borderColor:'white',
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center'
   },
   
});
