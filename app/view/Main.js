
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
  BackHandler,
  Alert,
  ActivityIndicator
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Api from '../controller/api';

export default class Main extends Component {

  constructor(props){
    super(props);
    this.state={
      userID:'',
      indicator: false

    }
  }
   componentWillMount(){
    BackHandler.addEventListener('hardwareBackPress', function() {

      const scene = Actions.currentScene;
      // alert(scene)
      if (scene === 'drawer')
       {
        BackHandler.exitApp()
      }
      Actions.pop()
      return true
    });
    }

  componentDidMount(){
    AsyncStorage.getItem('user_id')
    .then((id) => {console.log(id)})

  }

  _getService(type){

    this.setState({indicator:true})
    var formData = new FormData()
     formData.append('service_type',type);
 
     Api.post('app/api/getServiceList',formData)
     .then(result => {
   
       service = result.service_list
       console.log(service)
       
       if(type == 'Clothes')
       {
        this.setState({indicator:false})
        Actions.clothes({service})
       }
       else if(type == 'Carpets')
       {
        this.setState({indicator:false})
        Actions.carpets({service})
       }
       else if(type == 'Blanket')
       {
        this.setState({indicator:false})
        Actions.blankets({service})
       }
       else if(type == 'Curtains')
       {
        this.setState({indicator:false})
        Actions.curtains({service})
       }
       else if(type == 'Shoes')
       {
        this.setState({indicator:false})
        Actions.shoes({service})
       }
       else 
       {
        this.setState({indicator:false})
        Actions.cover({service})
       }
       
     })
     .catch( error => {console.error(error)});
 
   }

  render() {
    return (
      <View style={styles.container}>
      {this.state.indicator == true ? <View style={{ flex: 1,justifyContent: 'center'}}><ActivityIndicator size="large" color="white" /></View> :
      <ScrollView style={{height:'100%'}}>
      <View style={{marginTop:25,alignSelf:'center'}}> 
        <Text style={styles.title}>CHOOSE YOUR SERVICE</Text>
      </View>
      <View style={styles.view1}>
        <TouchableOpacity style={styles.box} onPress={()=> this._getService('Clothes')}>
          <Image source={require('../images/products/clothes.png')} style={{width:35,height:60,alignSelf:'center'}}/>
          <Text style={styles.boxText}>Clothes</Text>
        </TouchableOpacity >
        <TouchableOpacity style={styles.box} onPress={()=> this._getService('Carpets')}>
          <Image source={require('../images/products/carpet.png')} style={styles.image}/>
          <Text style={styles.boxText}>Carpets</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.view}>
       <TouchableOpacity  style={styles.box} onPress={()=> this._getService('Blanket')}>
            <Image source={require('../images/products/blanket.png')} style={styles.image}/>
            <Text style={styles.boxText}>Blanket</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.box} onPress={()=> this._getService('Curtains')}>
          <Image source={require('../images/products/curtains.png')} style={styles.image}/>
          <Text style={styles.boxText}>Curtains</Text>
        </TouchableOpacity>
        </View>
        <View style={styles.view}>
        <TouchableOpacity style={styles.box} onPress={()=> this._getService('Shoes')}>
            <Image source={require('../images/products/shoes.png')} style={styles.image}/>
            <Text style={styles.boxText}>Shoes</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.box} onPress={()=> this._getService('Cover')}>  
            <Image source={require('../images/products/cover3.png')} style={styles.image}/>
            <Text style={styles.boxText}>Cover</Text>
        </TouchableOpacity>
        </View>
        </ScrollView>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:15
   
  },
  box:{
    backgroundColor:'#fa6c70',
    justifyContent:'center',
    width:'45%',
    height:200,
    margin:5,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  btn:{
    marginTop:1.5,
    flexDirection:'row', 
    width:'100%',
    height:50,
    backgroundColor:'black',
    borderWidth:2,
    borderColor:'white',
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center'
   },
   view1:{
    flexDirection:'row',
    margin:10,
    marginTop:20,
    justifyContent:'space-between'
   },
   view:{
    flexDirection:'row',
    margin:10,
    marginTop:-5,
    justifyContent:'space-between'
   }
 
});
