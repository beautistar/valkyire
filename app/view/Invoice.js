
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  TextInput,
  AsyncStorage
} from 'react-native';
//import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';
import Header from '../components/Header';
import Api from '../controller/api';
import { Actions } from 'react-native-router-flux';

export default class Invoice extends Component {

    constructor(props){
        super(props);
        this.state={
            code:'',
            UserName:'',
            address:'',
            phone_number:'',
            discount:0,
            off_discount:0,
            grand_Total: 0,
            invoice:0,
            item:this.props.item,
            price: this.props.price,
            orderList:this.props.order
        }
        console.log('ORDERLIST',this.state.orderList)
        this._offer = this._offer.bind(this)
    }

    _offer(){
        AsyncStorage.getItem('user_id')
        .then((id) => {

        console.log(id)
        var formData = new FormData()
        formData.append('user_id',id);
        Api.post('app/api/getMyOffCode',formData)
        .then(result => {console.log('result',result)
            if(result.message == 'Success.')
            {
                if(result.off_code == this.state.code)
                {  
                   this.setState({off_discount:result.discount},()=>console.log('state'))
                   //alert(this.state.off_discount)
                }
                else
                {
                    this.setState({off_discount:0},()=>console.log('state'))
                    //alert(this.state.off_discount)
                }
                this.setState({discount:(this.state.price/100) * this.state.off_discount})
            }
            else
            {
                //this.setState({off_discount:5})
                console.log(result.message)
            }
        })
        .catch(error => {console.error(error)});
    })
    }

    componentDidMount(){
        console.log('didmount')
    
        AsyncStorage.getItem('user_name')
        .then((name) => {this.setState({userName:name})})
   
        AsyncStorage.getItem('address')
        .then((add) => {this.setState({address:add})})

        AsyncStorage.getItem('phone_number')
        .then((no) => {this.setState({phone_number:no})})

        //this.setState({tax:(this.state.price/100) * 5.5})
        var invoiceNumber = Date.now().toString()
        this.setState({invoice:invoiceNumber})
    }

  render() {
    return (
      <View style={styles.container}>
        <Header />
        {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
        <ScrollView>
        <View style={styles.view1}>
            <Text style={styles.textView1}>INVOICE</Text>
        </View>
        <View style={styles.view4}> 
            <View style={{flexDirection:'row'}}>
                <View style={{margin:10,width:'40%'}}>
                    <Text style={{color:'#ea0017'}} >Invoice To</Text>
                    <Text style={{color:'black',fontSize:17,fontWeight:'500'}}>{this.state.userName}</Text>
                </View>
                <View style={{margin:10,width:'55%'}}>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Text style={{color:'#ea0017'}} >Invoice Date : </Text>
                        <Text style={{color:'black',fontSize:12}}>{new Date().toLocaleDateString()}</Text>
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Text style={{color:'#ea0017'}} >Invoice No : </Text>
                        <Text style={{color:'black',fontSize:Platform.OS == 'android' ? 12 : 11}}>{this.state.invoice}</Text>
                    </View>
                </View>
            </View>
            <View style={{flexDirection:'row'}}>
                <View style={{marginLeft:10,marginTop:10,width:'45%'}}>
                    <View>
                        <Text style={{color:'black'}}>Address :</Text>
                        <Text style={{color:'#ea0017'}}>{this.state.address == 'null' ? 'Please add Address' : this.state.address}</Text>
                    </View>
                    <View style={{marginTop:5}}>
                        <Text style={{color:'black'}}>Phone :</Text>
                        <Text style={{color:'#ea0017'}}>{this.state.phone_number == null ? 'Please add Phone No' : this.state.phone_number}</Text>
                    </View>
                </View>
                <View style={{marginTop:10,width:'50%'}}>
                    <Text style={{fontSize:25,fontWeight:'500',color:'black'}}>Total Due :</Text>
                    <Text style={{fontSize:25,fontWeight:'500',color:'#ea0017'}}>{(this.state.price - Number(this.state.discount)).toFixed(2)} JOD</Text>
                </View>
            </View>
            <View style={{alignSelf:'flex-end',width:'55%',flexDirection:'row',marginTop:10,paddingLeft:10,alignItems:'center'}}>
                    <Text style={{color:'black',fontSize:15,fontWeight:'500'}} >Code : </Text>
                    <TextInput style={{width:100}} onEndEditing={() => this._offer()}  placeholder='#60087' onChangeText={(text) => {this.setState({code:text})}} />
            </View>
            <View style={styles.view5}>
                <View style={{width:'35%',alignItems:'center',flexDirection:'row',justifyContent:'center'}}>
                    <Text style={{color:'white',fontSize:10}} >Item Description </Text>
                    <Image source={require('../images/products/invoiceArrow.png')} style={{marginLeft:'10%',height:12,width:12}} />
                    
                </View>
                <View style={{width:'25%',alignItems:'center',flexDirection:'row',justifyContent:'center'}}>
                    <Text style={{color:'white',fontSize:10}}>Unit Price </Text>
                    <Image source={require('../images/products/invoiceArrow.png')} style={{marginLeft:'10%',height:12,width:12}} />
                    
                </View>
                <View style={{width:'20%',alignItems:'center',flexDirection:'row',justifyContent:'center'}}>
                    <Text style={{color:'white',fontSize:10}}>Quantity </Text>
                    <Image source={require('../images/products/invoiceArrow.png')} style={{marginLeft:'10%',height:12,width:12,}} />
                    
                </View>
                <View style={{width:'20%',alignItems:'center',flexDirection:'row',justifyContent:'center'}}>
                    <Text style={{color:'white',fontSize:10}}>Total </Text>
                </View>
            </View>
            <FlatList data = {this.state.orderList}
                      renderItem = {({item}) => 
                      <View style={styles.view6}>
                      <View style={{width:'35%'}}>
                          <Text style={{color:'black',fontSize:10,textAlign:'center'}}>
                                {item.wash == true && item.iron == false ? 'WASHING' 
                                : item.iron == true && item.wash == false ?  'IRONING' 
                                : item.wash == true && item.iron == true ? 'WASHING & IRONING' 
                                : 'WASHING & IRONING'}</Text>
                          <Text style={{fontSize:10,textAlign:'center'}}>{item.name}</Text>
                      </View>
                      <View style={{width:'25%'}}>
                          <Text style={{color:'red',fontSize:10,textAlign:'center'}}>JOD {item.price} </Text>
                         
                      </View>
                      <View style={{width:'20%'}}>
                          <Text style={{color:'red',fontSize:10,textAlign:'center'}}>{item.qty} </Text>
                         
                      </View>
                      <View style={{width:'20%'}}>
                          <Text style={{color:'red',fontSize:10,textAlign:'center'}}>{item.price * item.qty} JOD </Text>
                      </View>
                  </View> } 
                  keyExtractor = {(item,index) => index.toString()}
                    />
            
            <View style={{borderTopWidth:0.5}}>
                <View style={{flexDirection:'row',justifyContent:'flex-end',padding:5}}>
                    <Text style={{color:'#ea0017'}} >Sub Total : </Text>
                    <Text style={{color:'black'}}>{this.state.price} JOD</Text>
                </View>
                {/* <View style={{flexDirection:'row',justifyContent:'flex-end',padding:5}}>
                    <Text style={{color:'#ea0017'}} >Tax(Vat 5.5%) : </Text>
                    <Text style={{color:'black'}}>{(this.state.tax).toFixed(2)} JOD</Text>
                </View> */}
                <View style={{flexDirection:'row',justifyContent:'flex-end',padding:5}}>
                    <Text style={{color:'#ea0017'}} >Discount({this.state.discount == 0 ? 0 : this.state.off_discount}%) : </Text>
                    <Text style={{color:'black'}}>{(this.state.discount).toFixed(2)} JOD</Text>
                </View>
                <View style={{flexDirection:'row',alignSelf:'flex-end',padding:5,marginTop:10,marginBottom:10,backgroundColor:'#ea0017'}}>
                    <Text style={{fontWeight:'500',fontSize:17,color:'white'}}>Grand Total : </Text>
                    <Text style={{fontWeight:'500',fontSize:17,color:'white'}}>{(this.state.price - Number(this.state.discount)).toFixed(2)} </Text>
                </View>
            </View>
        </View>
        <View style={{alignItems:'center',padding:10}}>
                    <Text style={{fontSize:15}}>THANK YOU FOR YOUR BUSINESS</Text>
        </View>
        <View style={styles.view7}>
                <View style={{flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
                    <View style={styles.view} >
                        <View>
                            <Image source={require('../images/products/website.png')} style={styles.image3}/>
                        </View>
                        <View style={{marginLeft:5,flexWrap:'wrap'}}>
                            <Text style={styles.textView3} >www.valkyreilaundry.com</Text>
                        </View>
                    </View>
                    <View style={styles.view}>
                        <View>
                            <Image source={require('../images/products/location.png')} style={styles.image3}/>
                        </View>
                        <View style={{marginLeft:5,flexWrap:'wrap'}}>
                            <Text style={styles.textView3}>Amman jo Queen Rania st</Text>
                        </View>
                    </View>
                    <View style={styles.view}> 
                        <View>
                            <Image source={require('../images/products/call.png')} style={styles.image3}/>
                        </View>
                        <View style={{marginLeft:5,flexWrap:'wrap'}}>
                            <Text style={styles.textView3}>00962789340608</Text>
                           
                        </View>
                    </View>
                </View>
        </View>
        </ScrollView>
        <TouchableOpacity onPress={() => Actions.payments({item:this.state.item,price:(this.state.price - Number(this.state.discount)).toFixed(2),invoice:this.state.invoice,code:this.state.code})} style={styles.view2}>
            <Text style={styles.textView2}>CONFIRMATION</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  header: {
    backgroundColor:'#ffffff',
    elevation:0,

  },
  title:{
    color:'white',
    fontWeight:'700',
    fontSize:20
  },
  box:{
    margin:5,
    width:180,
    height:250,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    textAlign:'center',
    top:120,
    color:'white',
    fontWeight:'500',
    fontSize:15
  },
  activeBox:{
    margin:5,
    width:180,
    height:250,
    backgroundColor:'white'
  },
  activeBoxText:{
    textAlign:'center',
    top:120,
    color:'#ea0017',
    fontWeight:'500',
    fontSize:15
  },
  textView3:{
    fontSize:8
  },
  view:{
    width:'28%',
    flexDirection:'row',
    alignItems:'center',
    marginRight:Platform.OS == 'android' ? 5 : 10
  },
  image3:{
    height:15,
    width:15
  },
  view2:{
    backgroundColor:'#000000',
    height:50,
    justifyContent:'center'
  },
  textView2:{
    color:'white',
    fontWeight:'700',
    fontSize:22,
    textAlign:'center'
  },
  view1:{
    backgroundColor:'#ffffff',
    justifyContent:'center',
    padding:15
  },
  textView1:{
    fontSize:35,
    fontWeight:'bold',
    textAlign:'center',
    color:'black',
    fontFamily:Platform.OS ==  'android' ? 'HelveticaBold' :null
  },
  view4:{
    borderWidth:1,
    width:'90%',
    alignSelf:'center'
  },
  view5:{
    marginTop:30,
    height:25,
    flexDirection:'row',
    backgroundColor:'#ea0017'
  },
  view6:{
    marginTop:5,
    marginBottom:5,
    height:25,
    flexDirection:'row'
  },
  view7:{
    alignSelf:'center',
    padding:10
  },
  headerImage:{
    width:90,
    height:60,
    alignSelf:'center'
  }
});
