
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView
} from 'react-native';
//import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';
import AppHeader from '../components/Header';
import { Actions } from 'react-native-router-flux';

export default class Payments extends Component {
  render() {
    return (
      <View style={styles.container}>
            <AppHeader/>
            {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
                                        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
           <ScrollView>
            <View style={{alignItems:'center',padding:40}}>
                <View style={{borderColor:'white',borderWidth:1,borderRadius:70,alignItems:'center',justifyContent:'center',height:120,width:120}} >
                    <Image source={require('../images/products/done.png')} style={{height:25,width:35}} />
                </View>
                <Text style={{color:'white', fontFamily:Platform.OS == 'android' ? 'HelveticaBold' : null,fontSize:20}}>DONE</Text>
            </View>
            <View style={{alignSelf:'center',padding:20}}>
                <Text style={{color:'white',fontFamily:Platform.OS == 'android' ? 'HelveticaBold' : null, fontSize:15,textAlign:'center'}} >THANKS FOR CHOOSING US OUR DRIVER WILL ARRIVE SHORTLY </Text>
            </View>
            <View style={{alignItems:'center',padding:30}}>
                <Image source={require('../images/products/smily.png')} style={{height:50,width:50}} />
            </View>
            </ScrollView>
            <TouchableOpacity style={{bottom:5,position:'absolute', width:'30%',padding:5,borderWidth:2,borderColor:'white',justifyContent:'center',alignItems:'center',alignSelf:'center'}} onPress={() => Actions.drawer()}>
                <Text style={{color:'white'}}>OK</Text>
            </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  header: {
    backgroundColor:'#ffffff'
  },
  title:{
    color:'white',
    fontWeight:'700',
    fontSize:20
  },
  box:{
    margin:5,
    width:180,
    height:250,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    textAlign:'center',
    top:120,
    color:'white',
    fontWeight:'500',
    fontSize:15
  },
  activeBox:{
    margin:5,
    width:180,
    height:250,
    backgroundColor:'white'
  },
  activeBoxText:{
    textAlign:'center',
    top:120,
    color:'#ea0017',
    fontWeight:'500',
    fontSize:15
  },
  headerImage : {
    width:90,
    height:60,
    alignSelf:'center'
  },
  view1 :{
    justifyContent:'space-between',
    alignItems:'center',
  },
  view2:{
    backgroundColor:'#000000',
    width:'85%',
    flexDirection:'row',
    padding:35,
    justifyContent:'center'
  },
  view3:{
    backgroundColor:'#ffffff',
    width:'85%',
    flexDirection:'row',
    marginTop:20,
    padding:35,
    justifyContent:'center'

  },
  view4:{
    backgroundColor:'#000000',
    height:50,
    justifyContent:'center'
  },
  text1:{
    color:'white',
    fontSize:20,
    fontWeight:'500',
    textAlign:'center'
    
  },
  text2:{
    color:'red',
    fontSize:20,
    fontWeight:'500',
    textAlign:'center'
    
  },
  text3:{
    color:'white',
    fontWeight:'700',
    fontSize:22,
    textAlign:'center'
  }
 
});
