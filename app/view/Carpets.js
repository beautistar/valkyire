import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  ScrollView,
  AsyncStorage,
  StatusBar
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Header from '../components/Header';


var service;
var carpets_items = [];
var orderList = [];
var name = [] , name1 = [];

export default class Carpets extends Component {
  constructor(props){
    super(props);
    this.state={
        item:0,
        price:0,
        iron:false,
        washing:false,
        q:''
      
    }
  }
  wash(id){
    items = this.state.q
        for(i=0;i<items.length;i++)
        {
          if(items[i].id == id)
          {
            items[i].wash = !items[i].wash
          }
        }
      this.setState({q:items})
      console.log(this.state.q)
      AsyncStorage.setItem('carpet_qty',JSON.stringify(this.state.q))
      //this.props.total(this.state.item ,this.state.price)
      console.log(this.state.q)


      AsyncStorage.getItem('carpetList')
      .then((result) => {
            orderList = JSON.parse(result)
            for(i=0;i<orderList.length;i++)
              {
                if(orderList[i].id == id)
                {
                  orderList[i].wash = !orderList[i].wash
                }
              }
            AsyncStorage.setItem('carpetList',JSON.stringify(orderList))
            console.log('data',orderList)
    })
   }
  
   iron(id){
    items = this.state.q
        for(i=0;i<items.length;i++)
        {
          if(items[i].id == id)
          {
            items[i].iron = !items[i].iron
          }
        }
      this.setState({q:items})
      console.log(this.state.q)
      AsyncStorage.setItem('carpet_qty',JSON.stringify(this.state.q))
      //this.props.total(this.state.item ,this.state.price)
      console.log(this.state.q)

      AsyncStorage.getItem('carpetList')
      .then((result) => {
            orderList = JSON.parse(result)
            for(i=0;i<orderList.length;i++)
              {
                if(orderList[i].id == id)
                {
                  orderList[i].iron = !orderList[i].iron
                }
              }
            AsyncStorage.setItem('carpetList',JSON.stringify(orderList))
            console.log('data',orderList)
    })
   }

  componentWillMount(){
     console.log('willmount')
    let service = this.props.service
      service == undefined ? null :
      service.map((item,index) => 
      {
        carpets_items[index] = {id:item.service_id, qty:0, wash:false, iron:false} 
      })
      this.setState({q:carpets_items})
      console.log('CARPET',this.state.q)
  }

  componentDidMount(){

    AsyncStorage.getItem('totalitem')
    .then(item => {this.setState({item:Number(item)})})
 
    AsyncStorage.getItem('totalprice')
    .then(item => {this.setState({price:Number(item)})})

    //this.setState({item:this.props.total_item , price: this.props.total_price})
    AsyncStorage.getItem('carpet_qty')
    .then(result => {
        if(result != null )
        {
          this.setState({q:JSON.parse(result)})
        }
        else
        {
          this.setState({q:carpets_items})
          console.log(this.state.q)
        }

    })
  }

  total(id,price , remove){
    //var totalitem=0 , totalprice=0
    //totalitem = this.props.total_item , totalprice = this.props.total_price
    if(this.state.item >= 0 ){
        if(remove == false)
    {
         
        this.state.price = this.state.price + Number(price) 
        this.state.item = this.state.item + 1

    }
    else
    {
        this.state.price = this.state.price - Number(price) 
        this.state.item = this.state.item - 1

    }
    }
    AsyncStorage.multiSet([['totalitem',this.state.item.toString()],['totalprice',this.state.price.toString()]])
    AsyncStorage.getItem('carpetList')
    .then((result) => {
            orderList = JSON.parse(result)
            orderList.map((item,index) => {
                if(item.id == id)
                {
                console.log('remove',remove)
                if(remove == false)
                {
                    orderList[index].qty = orderList[index].qty + 1
                   // console.log(orderList[index])
                }
                else
                {
                    orderList[index].qty = orderList[index].qty - 1
                }
                
                } 
            }
            )
            AsyncStorage.setItem('carpetList',JSON.stringify(orderList))
            console.log('data',orderList)
    })
  }      


add(id, index, price){
   // this.setState({ item: this.state.item + 1 , price : this.state.price + Number(price) })
    //this.props.total(this.state.item,this.state.price)
    items = this.state.q
    for(i=0;i<items.length;i++)
    {
      if(items[i].id == id)
      {
        items[i].qty = items[i].qty + 1
      }
    }
    this.setState({q:items})
    AsyncStorage.setItem('carpet_qty',JSON.stringify(this.state.q) )
    console.log(this.state.q)
     this.total(id,price, false)
    }

  remove(id, index, price ){
      if(this.state.q[index].qty != 0)
      {
        //   this.setState({ item: this.state.item - 1 , price : this.state.price - Number(price) })
        //   this.props.total(this.state.item,this.state.price)

          items = this.state.q
          for(i=0;i<items.length;i++)
          {
            if(items[i].id == id)
            {
              items[i].qty = items[i].qty - 1
            }
          }
          this.setState({q:items})
          AsyncStorage.setItem('carpet_qty',JSON.stringify(this.state.q) )

          console.log(this.state.q)
          this.total(id,price,true)

      }
      
  }
  render() {
    let service = this.props.service
    if(service != undefined)
    {
    for(i=0;i<service.length;i++)
      {

       orderList[i] = {price:service[i].price,name:service[i].service_name,id:service[i].service_id,image:service[i].photo_url,qty:0,wash:false, iron:false} 
       AsyncStorage.getItem('carpetList')
       .then((result) => {
            if(result == null)
            {
              AsyncStorage.setItem('carpetList',JSON.stringify(orderList))
            }
            
       } )
    }
  }
    return (
      <View style={styles.container}>
       <Header/>
      {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
      <ScrollView>
            {service == undefined ? <View></View> :
            <View  style={{ flexDirection: 'row', margin: 10, marginTop: 20, marginBottom: 10,flexWrap:'wrap',justifyContent:'center' }}>
                {service.map((item,index) => (

                <View key={index} style={styles.box}>
                    <Text style={styles.boxText}>{item.service_name}</Text>
                    <Image source={{ uri: item.photo_url }} style={{ width: 90, height: 100, alignSelf: 'center', marginTop: 10 }} />
                    <Text style={styles.h1}>{item.price} JOD</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                        <TouchableOpacity style={styles.btncricle} onPress={() => this.remove(item.service_id,index,item.price)}>
                            <Text style={styles.btncricleText}>-</Text>
                        </TouchableOpacity>
                        <Text style={styles.h2}>{ this.state.q != null ? this.state.q[index].qty : 0 + " Pec"}</Text>
                        <TouchableOpacity style={styles.btncricle} onPress={() => this.add( item.service_id,index,item.price)}>
                            <Text style={styles.btncricleText}>+</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                        <TouchableOpacity onPress={() => {this.setState({washing:!this.state.washing}),this.wash(item.service_id)}} style={{ width: 30, height: 30, borderRadius: 25, justifyContent: 'center', alignSelf: 'flex-start', margin: 10, backgroundColor: '#f98a8b' }} >
                            <Image source={this.state.q[index].wash == true ? require('../images/products/washing.png') : require('../images/products/washing1.png')} style={{ width: 15, height: 20, alignSelf: 'center' }} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {this.setState({iron:!this.state.iron}),this.iron(item.service_id)}} style={{ width: 30, height: 30, borderRadius: 25, justifyContent: 'center', alignSelf: 'flex-end', margin: 10, backgroundColor: '#f98a8b' }} >
                            <Image source={this.state.q[index].iron == true ? require('../images/products/ironing.png') : require('../images/products/ironing1.png') } style={{ width: 25, height: 23, alignSelf: 'center' }} />
                        </TouchableOpacity>
                    </View>
                </View>
                ))}
            </View> }
       </ScrollView>
       <View style={{flexDirection:'row',width:'86%',alignSelf:'center'}}>
         <View style={{backgroundColor:'black',height:50,width:'50%',justifyContent:'center',alignItems:'center'}}>
         <Text style={styles.text}>TOTAL ITEMS</Text>
         <Text style={styles.text}> {this.state.item} items</Text>
         </View>
         <View style={{backgroundColor:'white',width:'50%',justifyContent:'center',alignItems:'center'}}>
         <Text style={styles.text2}>TOTAL COST</Text>
         <Text style={styles.text2}> {this.state.price} JOD</Text>
         </View>
       </View>
       <TouchableOpacity style={styles.btn} onPress={()=> this.state.item == 0 ? alert('You must add item before you confirm order') : Actions.confirmOrder({totalItems:this.state.totalItems,totalPrice:this.state.totalPrice})}>
       <Text style={{color:'white',fontSize:20}}>CONFIRM YOUR ORDER</Text>
       </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   // justifyContent: 'center',
   // alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontWeight:'700',
    fontSize:20
  },
 
  activeTab:{
    backgroundColor:'black',
    color:'red',
   } ,
   tabText:{
    color:'white',
    //fontWeight:'bold'
   },
   tab:{
    justifyContent:'center',
    margin:10
   },
   text:{
    color:'white',
    fontWeight:'500',
    color:'white',
   },
   text2:{
    color:'white',
    fontWeight:'500',
    color:'#ea0017',
   },
   btn:{
    marginTop:1.5,
    flexDirection:'row', 
    width:'86%',
    height:50,
    borderWidth:2,
    borderColor:'white',
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center'
   },
   box: {
    margin: 2,
    width: '45%',
    borderWidth: 1,
    borderColor: 'white'
    },
    boxText: {
        marginTop: 10,
        textAlign: 'center',
        color: 'white',
        fontSize: 15
    },
    h1: {
        marginTop: 10,
        textAlign: 'center',
        color: 'white',
        fontSize: 10,
    },
    h2: {
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: 'center',
        color: 'white',
        fontSize: 10,
    },
    btncricle: {
        borderRadius: 10,
        borderWidth: 1,
        width: 20,
        height: 20,
        borderColor: 'white',
        alignItems: 'center'
    },
    btncricleText: {
        fontWeight: '700',
        color: 'white',
        fontSize: 15,
        marginTop: -1.5,
    },
   
});
