import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  ActivityIndicator,
  AsyncStorage
} from 'react-native';
import Api from '../controller/api';

export default class Home extends Component {

  constructor(props){
    super(props);
    this.state={
      indicator: false,
    }
  }

  _getService(type){
   this.setState({indicator:true})
   var formData = new FormData()
    formData.append('service_type',type);

    Api.post('app/api/getServiceList',formData)
    .then(result => {
  
      service = result.service_list
      console.log(service)
      this.setState({indicator:false})
      this.props.navigation.navigate(type,{service})
    })
    .catch( error => {console.error(error)});

  }

  render() {
    return (
      <View style={styles.container}>
       {this.state.indicator == true ? <View style={{ flex: 1,justifyContent: 'center'}}><ActivityIndicator size="large" color="#ffffff" /></View> :
      <ScrollView>
          <View style={{ marginTop: 25, alignSelf: 'center' }}>
            <Text style={styles.title}>CHOOSE YOUR SERVICE</Text>
          </View>
          <View style={{ flexDirection: 'row', margin: 10, marginTop: 20, justifyContent: 'center' }}>
            <TouchableOpacity style={styles.activeBox} onPress={() => this._getService('Clothes')}>
              <View >
                <Image source={require('../images/products/clothes1.png')} style={{ width: 38, height: 63, alignSelf: 'center' }} />
                <Text style={styles.activeBoxText}>Clothes</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.box} onPress={() => this._getService('Carpets')} >
              <View>
                <Image source={require('../images/products/carpet.png')} style={styles.image} />
                <Text style={styles.boxText}>CARPETS</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row', margin: 10, marginTop: -5, justifyContent: 'center' }}>
            <TouchableOpacity style={styles.box} onPress={() => this._getService('Blankets')} >
              <View>
                <Image source={require('../images/products/blanket.png')} style={styles.image} />
                <Text style={styles.boxText}>Blanket</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.box} onPress={() => this._getService('Curtains')}>
              <View>
                <Image source={require('../images/products/curtains.png')} style={styles.image} />
                <Text style={styles.boxText}>CURTAINS</Text>
              </View>
            </TouchableOpacity>
          </View> 
        </ScrollView>
      }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:15
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  }
 
});
