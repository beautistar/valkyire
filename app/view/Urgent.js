
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';
//import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../components/Header';
import Api from '../controller/api'
export default class Urgent extends Component {
    constructor(props){
      super(props);
      this.state={
        userID:'',
        userName:'',
        address:'',
        urgent:false,
        regular:true,
        item:0,
        price:0,
        order:this.props.order,
        indicator: false
      }
    }

    componentDidMount(){

      let totalItems=this.props.item
      let totalPrice= this.props.price
      this.setState({item:totalItems,price:totalPrice})
      console.log('price',this.props.price)
    }

    urgent=()=>{
      let totalItems=this.props.item
      let totalPrice= this.props.price
      var totalPrice1=0
      var urgentTrue=true
      console.log(totalItems,totalPrice)
      this.setState({urgent:true,regular:false})
      if(urgentTrue === true)
          {    
            
            totalPrice1=(totalPrice*2)
            console.log("total : ",totalPrice1)            

          }
      console.log(totalPrice1)
      this.setState({item:totalItems,price:totalPrice1},() => {console.log(this.state.item,this.state.price)})
      
    }

    regular=()=>{
      let totalItems=this.props.item
      let totalPrice= this.props.price
      var totalPrice1=0
      var regularTrue=true
      console.log(totalItems,totalPrice)
      this.setState({urgent:false,regular:true})
            if(regularTrue === true)
          {    
            
            totalPrice1=totalPrice
            console.log("total : ",totalPrice1)            

          }
      console.log(totalPrice1)
      this.setState({item:totalItems,price:totalPrice1},() => {console.log(this.state.item,this.state.price)})
      
    }

  render() {
    return (
      <View style={styles.container}>
         
          <AppHeader/>
          
          {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
          <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>}    
           {this.state.indicator == true ? <View style={{ flex: 1,justifyContent: 'center'}}>
              <ActivityIndicator size="large" color="#ea0017" /></View> :
        <ScrollView>
        <View style={{marginTop:'10%'}}>
            <View style={styles.view1} >
                <TouchableOpacity onPress={this.urgent} style={this.state.urgent == true ? styles.view3 : styles.view2}>
                        <Text style={this.state.urgent == true ? styles.text2 : styles.text1}>URGENT</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.regular} style={this.state.regular == false ? styles.view2 : styles.view3 } >
                        <Text style={this.state.regular == false ? styles.text1 : styles.text2}>REGULAR</Text>
                </TouchableOpacity>
            </View>
        </View>
        <View style={{width:'85%',alignSelf:'center',marginTop:'10%',height:100}}>
            <Text style={{color:'white',fontSize:22,textAlign:'center'}} >if you choose urgent it will increase double price</Text>
        </View>
      </ScrollView> }
        <TouchableOpacity style={styles.view4} onPress={() => Actions.invoice({order:this.state.order,item:this.state.item,price:this.state.price})} >
            <Text style={styles.text3}>Continue</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
    justifyContent:'space-between'
  },
  header: {
    backgroundColor:'#ffffff'
  },
  title:{
    color:'white',
    fontWeight:'700',
    fontSize:20
  },
  box:{
    margin:5,
    width:180,
    height:250,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    textAlign:'center',
    top:120,
    color:'white',
    fontWeight:'500',
    fontSize:15
  },
  activeBox:{
    margin:5,
    width:180,
    height:250,
    backgroundColor:'white'
  },
  activeBoxText:{
    textAlign:'center',
    top:120,
    color:'#ea0017',
    fontWeight:'500',
    fontSize:15
  },
  headerImage : {
    width:90,
    height:60,
    alignSelf:'center'
  },
  view1 :{
    justifyContent:'space-between',
    alignItems:'center',
  },
  view2:{
    backgroundColor:'#000000',
    width:'85%',
    flexDirection:'row',
    marginTop:20,
    padding:35,
    justifyContent:'center'
  },
  view3:{
    backgroundColor:'#ffffff',
    width:'85%',
    flexDirection:'row',
    marginTop:20,
    padding:35,
    justifyContent:'center'

  },
  view4:{
    backgroundColor:'#000000',
    height:50,
    justifyContent:'center'
  },
  text1:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ?'500' : 'bold',
    textAlign:'center'
    
  },
  text2:{
    color:'red',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ?'500' : 'bold',
    textAlign:'center'
    
  },
  text3:{
    color:'white',
    fontWeight:'700',
    fontSize:22,
    textAlign:'center'
  }
 
});
