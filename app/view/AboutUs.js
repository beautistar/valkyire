import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
  FlatList,
  ActivityIndicator
} from 'react-native';
import Api from '../controller/api';
import { Actions } from 'react-native-router-flux';

export default class AboutUs extends Component {
    constructor(props){
        super(props);
        this.state={
            userID:'',
            distinguish:'',
            privacy:'',
            termsCondition:'',
            indicator: false

        }
    }

  componentDidMount(){
     
      this.setState({indicator:true})
      Api.post('app/api/getAboutUs')
      .then(result => {
        console.log(result)
        this.setState({distinguish:result.distinguish_us,privacy:result.privacy_policy,termsCondition:result.terms_conditions})
        this.setState({indicator:false})
      })
      .catch( error => {console.error(error)});

      
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{backgroundColor:'white',alignItems:'center',width:'100%', flex:1,marginTop:5}}>
            <View style={{justifyContent:'space-between',width:'100%',alignItems:'center',padding:20}}>
                
                <TouchableOpacity style={styles.btn} onPress={() => Actions.distinguish({about:this.state.distinguish})}>
                    <Text style={styles.btnText}>Distinguish Us</Text>
                </TouchableOpacity>
                
                <TouchableOpacity style={styles.btn} onPress={() => Actions.privacyPolicy({privacy:this.state.privacy})}>
                    <Text style={styles.btnText}>Privacy Policy</Text>
                </TouchableOpacity>
                
                <TouchableOpacity style={styles.btn} onPress={() => Actions.termsAndConditions({terms:this.state.termsCondition})}>
                    <Text style={styles.btnText}>Terms and Conditions</Text>
                </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:15
  },
  btn:{
    marginTop:20,
    backgroundColor:'#ea0017',
    height:40,
    width:'90%',
    justifyContent:'center',
    borderRadius:5
  },
  btnText:{
    color:'white',
    fontSize:16,
    fontWeight:Platform.OS == 'android' ? '500' : '800',
    textAlign:'center'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  list: {
    flexDirection:'row',
    padding:15,
    borderBottomWidth:1,
    borderBottomColor:'grey',
    backgroundColor:'white'
  },
 
});
