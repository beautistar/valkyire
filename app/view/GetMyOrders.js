import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
  FlatList
} from 'react-native';
import Api from '../controller/api';
import { Actions } from 'react-native-router-flux';

export default class GetMyOrders extends Component {
    constructor(props){
        super(props);
        this.state={
            userID:'',
            data:''
        }
    }

  componentDidMount(){
    var usr;
    AsyncStorage.getItem('user_id')
    .then((id) =>{
      
      usr=id
      console.log('id',usr)

      var formData = new FormData()
      formData.append('user_id',id);
    
      Api.post('app/api/getMyOrders',formData)
      .then((result) => {
        console.log(result)
        this.setState({data:result.order_list})
      })
      .catch( error => {console.log(error)});
  
  })    

  }

  render() {
    return (
      <View style={styles.container}>
      <ScrollView>
         <View style={{flexDirection:'row',padding:15,backgroundColor:'#ea0017'}}>
            <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'white',fontSize:15,fontWeight:'500'}}>Order Id</Text>
            </View>
            <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'white',fontSize:15,fontWeight:'500'}}>Total Items</Text>
            </View>
            <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'white',fontSize:15,fontWeight:'500'}}>Total Cost</Text>
            </View>
            <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'white',fontSize:15,fontWeight:'500'}}>Date</Text>
            </View>
         </View>
         <FlatList 
           data={this.state.data}
           renderItem={({item}) => 
            <TouchableOpacity>
                <View style={styles.list}>
                    <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                    <Text>{item.order_id}</Text>
                    </View>
                    <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                        <Text>{item.total_itmes}</Text>
                    </View>
                    <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                        <Text>{item.total_cost}</Text>
                    </View>
                    <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                        <Text>{  new Date( item.ordered_at.slice(0,10) ).toLocaleDateString() }</Text>
                    </View>
                </View>
            </TouchableOpacity>

            
            }
            keyExtractor={(item,index) => item.order_id}
         />
      </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:15
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  list: {
    flexDirection:'row',
    padding:15,
    borderBottomWidth:1,
    borderBottomColor:'grey',
    backgroundColor:'white'
  },
 
});
