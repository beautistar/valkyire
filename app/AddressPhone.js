
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar,
  AsyncStorage,
  BackHandler,
  Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon} from 'native-base';
import Header from './components/Header1';
import Api from './controller/api';
import Geocoder from 'react-native-geocoding';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

var lat=0
var long=0
var gpsclick=false
export default class AddressPhone extends Component {
  constructor(props){
    super(props);
    this.state={
      address:'',
      phone:'',
      emailnull:this.props.email,
      email:this.props.email,
      userID:''
    }

    //console.log(this.props.user_name,this.props.email)
  }
  componentWillMount(){

    Geocoder.init('AIzaSyCAOFImrlK9C7-wmmqxz44KTs8ebW8BCHg');
      if(Platform.OS === 'android'){
        LocationServicesDialogBox.checkLocationServicesIsEnabled({ 
          message: "<h2>Use Location?</h2> \
                      This app wants to change your device settings:<br/><br/>\
                      Use GPS for location<br/><br/>", 
          ok: "YES", 
          cancel: "NO" 
      }).then(() => { 
        navigator.geolocation.getCurrentPosition((position) => {
          lat = parseFloat(position.coords.latitude)
          long = parseFloat(position.coords.longitude)
         console.log('lat',lat)
         console.log('long',long)
       
            }, (error) => console.log(JSON.stringify(error)),
         { enableHighAccuracy:true, timeout: 20000 })
      })
      }
      else{
        navigator.geolocation.getCurrentPosition((position) => {
          lat = parseFloat(position.coords.latitude)
          long = parseFloat(position.coords.longitude)
         console.log('lat',lat)
         console.log('long',long)
       
            }, (error) => console.log(JSON.stringify(error)),
         { enableHighAccuracy:true, timeout: 20000 })
      }

  }

  _getAddress(){
     gpsclick=true
      // fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+lat + ',' + long)
      //  .then((response) =>  response.json())
      //  .then((result) => {
          
      //      console.log(result)
      //      this.setState({address:result.results[0].formatted_address})
      //      console.log(this.state.address)
           
      // })

      Geocoder.init('AIzaSyCAOFImrlK9C7-wmmqxz44KTs8ebW8BCHg');
      if(Platform.OS === 'android'){
        LocationServicesDialogBox.checkLocationServicesIsEnabled({ 
          message: "<h2>Use Location?</h2> \
                      This app wants to change your device settings:<br/><br/>\
                      Use GPS for location<br/><br/>", 
          ok: "YES", 
          cancel: "NO" 
      }).then(() => { 
      Geocoder.from(lat,long)
      .then(json => { console.log(json)
              var addressComponent = json.results[0].address_components[0];
              this.setState({address:json.results[0].formatted_address})
        console.log(addressComponent);
      })
      .catch(error => console.warn(error));
    })
    }
    else{
      Geocoder.from(lat,long)
      .then(json => { console.log(json)
              var addressComponent = json.results[0].address_components[0];
              this.setState({address:json.results[0].formatted_address})
        console.log(addressComponent);
      })
      .catch(error => {Alert.alert("Use Location?","Please Enable Your Location..!"),gpsclick=false});
    }
  }
   


  _confirmNumber(){
    if( gpsclick==false){
      Alert.alert('Use Location','Please enable your location service and use location icon in order to input your correct location.')  
    }
    else if(this.state.address  && this.state.phone ){
      
      Actions.confirmNumber({user_name:this.props.user_name,email:this.state.email,phone:this.state.phone,address:this.state.address,id:this.props.id,facebook:true})
    }
    
    
  }
  render() {
    return (
      <View style={styles.container}>
       <Header/>
        {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
        <View style={{marginTop:'20%'}}>
          <View style={{justifyContent:'center',alignItems:'center'}}>
           <Text style={{fontWeight:'400',fontSize:18}}>Please Add Phone No and Address</Text>
           <View style={{alignItems:'center',flexDirection:'row',width:'80%',marginLeft:50}}>
              <TextInput style={{width:'78%',height:40,marginTop:10}} value={this.state.address} onChangeText={(text)=>this.setState({address:text})} placeholder="Address" />
              <TouchableOpacity onPress={()=> this._getAddress()} ><Icon type="MaterialIcons" name="my-location" style={{ marginTop:10, padding:6,fontSize:22, color:'#ea0017'}} /></TouchableOpacity>
           </View>
           </View>
           <View style={{alignItems:'center',flexDirection:'row',marginLeft:Platform.OS == 'android' ? 18 : 14}}>
           <Text style={{paddingTop:8}} >+962 | </Text>
              <TextInput style={{width:'66%',height:40,marginTop:10}}  onChangeText={(text)=>this.setState({phone:'+962'+text})} placeholder="Phone No" />   
           </View>
           {
             this.state.emailnull == undefined ? 
           <View style={{alignItems:'center',flexDirection:'row',width:'80%',marginLeft:60}}>
              <TextInput style={{width:'79%',height:40,marginTop:10}}  onChangeText={(text)=>this.setState({email:text})} placeholder="Email" />   
           </View>  : null 
          }
          <View style={styles.btnView}>
            <TouchableOpacity style={
              this.state.emailnull == undefined?
                (!this.state.address || !this.state.phone || this.state.phone=='+962'|| !this.state.email) ? styles.disable : styles.btn
              :
              (!this.state.address || !this.state.phone || this.state.phone=='+962') ? styles.disable : styles.btn
              
              }  onPress={() => this._confirmNumber()} disabled={!this.state.address || !this.state.phone} >
              <Text style={{color:'white',fontSize:20,fontWeight:'500',textAlign:'center'}}>Continue</Text>
            </TouchableOpacity>
          </View> 
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor:'#ea0017'
  },
  image:{
    alignItems:'center',
    margin:20
  },
  inputView:{
    flexDirection:'row',
    padding:10,
    borderWidth:0.5,
    width:'70%',
    marginTop:10,
    borderRadius:10
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  btn:{
    backgroundColor:'#ea0017',
    height:40,
    width:'60%',
    justifyContent:'center',
    borderRadius:5,
    marginTop:10
  },
  disable:{
    backgroundColor: 'rgba(234, 0, 23, 0.5)',
    height:40,
    width:'60%',
    justifyContent:'center',
    borderRadius:5,
    marginTop:10
  }
});
