
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Header from './components/Header1';
import Api from './controller/api';
import { Toast } from 'native-base';

export default class ConfirmNumber extends Component {
  constructor(props){
    super(props);
    this.state={
      code:'',
      error:'',
      userID:this.props.id,
      indicator: false,
      user:''

    }
     console.log('id :',this.state.userID)
    // console.log('email',this.props.email)
    // console.log('phone_number',this.props.phone)
    // console.log('address',this.props.address)
    // console.log('facebook_id',this.props.id)
  }

  componentDidMount(){
    //this.setState({userID:this.props.id},()=>console.log(this.state.userID))
    if(this.props.facebook)
    {
      this._FBSignin()
    }
  }
  
  _FBSignin(){

    var formData = new FormData()
    formData.append('user_name',this.props.user_name);
    formData.append('email',this.props.email);
    formData.append('phone_number',this.props.phone);
    formData.append('password',null);
    formData.append('address',this.props.address);
    formData.append('facebook_id',this.props.id);

    //console.log('======',formData)

    Api.post('app/api/facebookSignin',formData)
    .then((resp) => 
    {
      console.log('=================',resp)
      AsyncStorage.multiRemove(['user_id','user_name','email','address','phone_number'])
      if(resp.result_code == 200){
        this.setState({userID:resp.user_id})
      }
      else
      {
        this.setState({userID:resp.user_model.user_id})
      }
       AsyncStorage.setItem('user_name',this.props.user_name)
       AsyncStorage.setItem('email',this.props.email)
       AsyncStorage.setItem('address',this.props.address)
       AsyncStorage.setItem('phone_number',this.props.phone)
     
      console.log('userid',this.state.userID)
      })
      .catch(error => {console.log(error)});

      
    }

  _verify(){

      var formData = new FormData()
      //formData.append('user_id',this.state.id);
      formData.append('user_id',this.state.userID)
      formData.append('verification_code',this.state.code);
      
      Api.post('app/api/verifyPhoneNumber',formData)
      .then(result => {console.log('result',result)

          if(result.message == 'Success.')
          {
              this.setState({indicator:false})
              AsyncStorage.setItem('user_id',this.state.userID.toString())
              alert('you verified successfully')
              Actions.drawer()
           
          }
          else
          {
            this.setState({indicator:false})
              this.setState({error: result.message})
          }

      })
      .catch(error => {console.error(error)});
   }

  render() {
    return (
      <View style={styles.container}>
       <Header/>
        {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
        {this.state.indicator == true ? <View style={{ flex: 1,justifyContent: 'center'}}><ActivityIndicator size="large" color="#ea0017" /></View> :
        <View style={{marginTop:'20%'}}>
          <View style={{justifyContent:'center',alignItems:'center'}}>
           <Text style={{fontWeight:'400',fontSize:20}}>Please Verify Your Phone Number</Text>
           <TextInput  style={{width:'70%',height:40,marginTop:30,fontSize:16}} onChangeText={(text)=>this.setState({code:text})} placeholder="Verification Code" />
           {this.state.error != null ? <Text style={{color:'red',marginTop:3}}>{this.state.error}</Text>: null}
          </View>
          <View style={styles.btnView}>
            <TouchableOpacity style={styles.btn} onPress={() => this._verify()} >
              <Text style={{color:'white',fontSize:20,fontWeight:Platform.OS == 'android' ? '500' : '900',textAlign:'center'}}>Confirm</Text>
            </TouchableOpacity>
          </View> 
        </View>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor:'#ea0017'
  },
  image:{
    alignItems:'center',
    margin:20
  },
  inputView:{
    flexDirection:'row',
    padding:10,
    borderWidth:0.5,
    width:'70%',
    marginTop:10,
    borderRadius:10
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  btn:{
    backgroundColor:'#ea0017',
    height:50,
    width:'70%',
    justifyContent:'center',
    borderRadius:5,
    marginTop:10
  }
});
